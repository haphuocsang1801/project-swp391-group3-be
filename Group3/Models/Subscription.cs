namespace Group3.Models
{
    public class Subscription
    {
        public int subscription_id { get; set; }
        public int fee_id { get; set; }
        
        public DateTime date_subscription { get; set; }
        
        public DateTime date_expiration { get; set; }
    }
}