namespace Group3.Models
{
    public class Exam
    {
        public int exam_id { set; get; }
        public int exam_total_questions { set; get; }
        public double exam_point { set; get; }
        public string exam_name { set; get; }
        public string exam_times { set; get; }
        public int teacher_id { set; get; }
        public string exam_description { set; get; }

    }
}