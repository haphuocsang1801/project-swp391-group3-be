﻿namespace Group3.Models
{
    public class Chapter
    {
        public int chapter_id { get; set; }
        public string chapter_name { get; set; }
    }
}
