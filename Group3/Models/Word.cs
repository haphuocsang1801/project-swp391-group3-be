﻿namespace Group3.Models
{
    public class Word
    {
        public int word_id { get; set; }
        public string word_face { get; set; }
        public string word_nghia { get; set; }
        public string word_IPA { get; set; }
        public string wordType_id { get; set; }
        //public string exam_audio
        public string word_phrase { get; set; }
        public int teacher_id { get; set; }

        public string word_image { get; set; }
    }
}
