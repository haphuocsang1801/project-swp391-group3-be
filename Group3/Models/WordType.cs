namespace Group3.Models
{
    public class WordType
    {
        public string wordtype_id { get; set; }
        
        public string wordtype_name { get; set; }
    }
}