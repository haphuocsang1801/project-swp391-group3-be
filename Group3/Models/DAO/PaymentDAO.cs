using System.Data.SqlClient;
using static Group3.DBContext;

namespace Group3.Models.DAO
{
    public class PaymentDAO
    {
        public int totalPayment(){
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT COUNT(*) FROM dbo.Payment";
                sqlCommand.CommandText = query;
                var reader= sqlCommand.ExecuteScalar();
                if(reader!=null){
                    return (int) reader;
                }
            }
            catch (Exception ex)
            {
            }
            return 0;
        }
        public List<Payment> getListUserPayment(){
            try
            {
                List<Payment> list = new List<Payment>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT * FROM dbo.Payment";
                sqlCommand.CommandText = query;
                using var reader= sqlCommand.ExecuteReader();
                if(reader != null){
                    while(reader.Read()){
                        list.Add(new Payment{
                            payment_id=reader.GetInt32(0),
                            method_id=reader.GetInt32(1),
                            payment_date= reader.GetDateTime(2),
                            user_id= reader.GetInt32(3),
                            fee_id=reader.GetInt32(4),
                        });
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {}
            return null;
        }
        public List<PaymentMethod> getListPaymentMethod(){
            try
            {
                List<PaymentMethod> list = new List<PaymentMethod>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT * FROM [MethodPayment]";
                sqlCommand.CommandText = query;
                using var reader= sqlCommand.ExecuteReader();
                if(reader != null){
                    while(reader.Read()){
                        list.Add(new PaymentMethod{
                            method_id=reader.GetInt32(0),
                            method_name=reader.GetString(1)
                        });
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {}
            return null;
        }
        public double getTotalEearningDay(DateTime date){
            double total=0;
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT SUM(f.fee) FROM dbo.Payment p JOIN dbo.Fee f ON p.fee_id=f.fee_id WHERE p.payment_date=@date";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@date", date);
                using var reader= sqlCommand.ExecuteReader();
                if(reader != null){
                    while(reader.Read()){
                        total=reader.GetDouble(0);
                    }
                }
                return total;
            }
            catch (Exception ex)
            {}
            return total;
        }
        public double getTotalEearning(){
            double total=0;
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT SUM(f.fee) FROM dbo.Payment p JOIN dbo.Fee f ON p.fee_id=f.fee_id ";
                sqlCommand.CommandText = query;
                using var reader= sqlCommand.ExecuteReader();
                if(reader != null){
                    while(reader.Read()){
                        total=reader.GetDouble(0);
                    }
                }
                return total;
            }
            catch (Exception ex)
            {}
            return total;
        }
        public List<Fee> getListFee(){
            try
            {
                List<Fee> list = new List<Fee>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT * FROM dbo.Fee";
                sqlCommand.CommandText = query;
                using var reader= sqlCommand.ExecuteReader();
                if(reader != null){
                    while(reader.Read()){
                        list.Add(new Fee{
                            fee_id=reader.GetInt32(0),
                            fee_name=reader.GetDouble(1),
                            fee_type=reader.GetString(2)
                        });
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {}
            return null;
        }
    }
}