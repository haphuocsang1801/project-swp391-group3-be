using System.Data.SqlClient;
using static Group3.DBContext;
namespace Group3.Models.DAO
{
    public class QuestionDAO
    {
        public Questions GetQuestionByID(int question_id)
        {
            try
            {
                Questions questions = new Questions();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from dbo.Questions where question_id = @question_id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@question_id", question_id);
                using var reader = sqlCommand.ExecuteReader();
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        questions.question_id = reader.GetInt32(0);
                        questions.questionType_id = reader.GetInt32(1);
                        questions.question_sentence = reader.GetString(2);
                        questions.word_id = reader.GetInt32(3);
                    }
                    return questions;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return null;
        }
        public List<Questions> GetQuestions()
        {
            try
            {
                List<Questions> QuestionList = new List<Questions>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from dbo.Questions";
                sqlCommand.CommandText = query;
                using var reader = sqlCommand.ExecuteReader();
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        QuestionList.Add(new Questions
                        {
                            question_id = reader.GetInt32(0),
                            questionType_id = reader.GetInt32(1),
                            question_sentence = reader.GetString(2),
                            word_id = reader.GetInt32(3)
                        });
                    }
                    return QuestionList;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return null;
        }
        public int totalQuestion(){
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT COUNT(*) FROM dbo.Questions";
                sqlCommand.CommandText = query;
                var reader= sqlCommand.ExecuteScalar();
                if(reader!=null){
                    return (int) reader;
                }
            }
            catch (System.Exception ex)
            {
            }
            return 0;
        }
    }
    }