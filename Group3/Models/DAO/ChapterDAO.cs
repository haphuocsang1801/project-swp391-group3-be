﻿using System.Data.SqlClient;
using static Group3.DBContext;
namespace Group3.Models.DAO
{
    public class ChapterDAO
    {
        public List<Chapter> GetChaptersCompleted(Notebook notebook)
        {
            try
            {
                List<Chapter> chapterlist = new List<Chapter>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from NotebookChapter nbc join Chapter chapt on nbc.chapter_id = chapt.chapter_id where notebook_id = @notebook_id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@notebook_id", notebook.notebook_id);
                using var reader = sqlCommand.ExecuteReader();
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        chapterlist.Add(new Chapter{
                            chapter_id = reader.GetInt32(2),
                            chapter_name = reader.GetString(3)
                        });
                    }
                }
                reader.Close();
                return chapterlist;
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }
        public Chapter GetChapterByID(int chapter_id)
        {
            try
            {
                Chapter chapter = new Chapter();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from Chapter where chapter_id = @chapter_id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@chapter_id", chapter_id);
                using var reader = sqlCommand.ExecuteReader();
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        chapter.chapter_id = reader.GetInt32(0);
                        chapter.chapter_name = reader.GetString(1);
                    }
                }
                reader.Close();
                return chapter;
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }
        public List<Chapter> GetChaptersByCourseId(int course_id)
        {
            List<Chapter> chapter_list = new List<Chapter>();
            using SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = SQL;
            string query = "select * from Chapter chapter join ChapterCourse chaptcourse On chapter.chapter_id = chaptcourse.chapter_id where course_id = @course_id";
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@course_id", course_id);
            using var reader = sqlCommand.ExecuteReader();
            if (reader != null)
            {
                while (reader.Read())
                {
                    chapter_list.Add(new Chapter
                    {
                        chapter_id = reader.GetInt32(0),
                        chapter_name = reader.GetString(1)
                    });
                }
                reader.Close();
                return chapter_list;
            }
            return null;
        }
        public bool DeleteAllChapterByCourseID(int id){
            try
            {
                //DELETE FROM dbo.ChapterCourse WHERE course_id
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "DELETE FROM dbo.ChapterCourse WHERE course_id=@id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@id", id);
                var reader = sqlCommand.ExecuteNonQuery();
                if(reader > 0){
                    return true;
                }
            }
            catch (System.Exception ex)
            {
            }
            return false;
        }
        public bool AddChaper(int total){
            using SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = SQL;
            string query = "INSERT dbo.Chapter (chapter_name ) VALUES (@name )";
            sqlCommand.CommandText = query;
            sqlCommand.Parameters.AddWithValue("@name", "Chapter "+(total+1));
            var reader = sqlCommand.ExecuteNonQuery();
            if(reader > 0){
                return true;
            }
            return false;
        }

        public void addCompletedChapter(Notebook notebook, Chapter chapter)
        {
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "insert into NotebookChapter(notebook_id, chapter_id) values(@notebook_id, @chapter_id)";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@notebook_id", notebook.notebook_id);
                sqlCommand.Parameters.AddWithValue("@chapter_id", chapter.chapter_id);
                var reader = sqlCommand.ExecuteNonQuery();
            }
            catch (System.Exception ex)
            {
            }
        }
        public int getIDChapterLaster(){
            try
            {
                //SELECT MAX(course_id) FROM dbo.Course
                int count=0;
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT MAX(chapter_id) FROM dbo.Chapter ";
                sqlCommand.CommandText = query;
                using var reader = sqlCommand.ExecuteReader();
                if (reader != null)
                {
                    while(reader.Read()){
                        count=reader.GetInt32(0);
                    }
                }
                return count;
            }
            catch (System.Exception ex)
            {}
            return -1;
        }
        public bool AddChaperWithCourse(int chapterID,int course_id){
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "INSERT dbo.ChapterCourse ( course_id, chapter_id ) VALUES (@courseID, @chapterID )";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@courseID", course_id);
                sqlCommand.Parameters.AddWithValue("@chapterID", chapterID);
                var reader = sqlCommand.ExecuteNonQuery();
                if(reader > 0){
                    return true;
                }
            }
            catch (System.Exception ex)
            {
            }
            return false;
        }
        public bool DeleteChapterByChapterId(int chapterID){
            //DELETE FROM dbo.Course WHERE course_id=
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "DELETE FROM dbo.ChapterCourse WHERE chapter_id=@chapterID";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@chapterID", chapterID);
                var reader = sqlCommand.ExecuteNonQuery();
                if(reader > 0){
                    return true;
                }
            }
            catch (System.Exception ex)
            {
                 // TODO
            }
            return false;
        }
        public bool DeleteChapter(int chapterID){
            //DELETE FROM dbo.Course WHERE course_id=
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "DELETE FROM dbo.Chapter WHERE chapter_id=@chapterID";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@chapterID", chapterID);
                var reader = sqlCommand.ExecuteNonQuery();
                if(reader > 0){
                    return true;
                }
            }
            catch (System.Exception ex)
            {
                 // TODO
            }
            return false;
        }
    }
}
