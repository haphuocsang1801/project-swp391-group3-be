﻿using System.Data.SqlClient;
using static Group3.DBContext;
namespace Group3.Models.DAO
{
    public class ReviewingDAO
    {
        public Questions GetQuestions(int word_id)
        {
            try{
                Questions question = new Questions();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from[Question] where word_id = @word_id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@word_id", word_id);
                using var reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        question.question_id = reader.GetInt32(0);
                        question.questionType_id = reader.GetInt32(1);
                        question.question_sentence = reader.GetString(2);
                        question.word_id=reader.GetInt32(3);
                    }
                    return question;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return null;
        }
        public int GetWordIDinNotebook(int notebook_id){
            try{
                Word word = new Word();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select distinct word_id from Notebook_word join NoteBookUser on Notebook_word.notebook_id=@notebook_id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@notebook_id", notebook_id);
                using var reader = sqlCommand.ExecuteReader();
                if(reader.HasRows){
                    while(reader.Read()){
                        word.word_id = reader.GetInt32(0);
                    }
                    return word.word_id;
                }
            }
            catch(Exception ex){

            }
            return -1;
        }
        public List<Questions> GetQuestionsByWordIdInNoteBook(int notebook_id)
        {
            try
            {
                List<Questions> question = new List<Questions>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select q.question_id, q.questionType_id, q.question_sentence, q.word_id from Questions q join Notebook_word n on q.word_id=n.word_id where n.notebook_id=@notebook_id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@notebook_id", notebook_id);
                using var reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        question.Add(new Questions
                        {
                            question_id = reader.GetInt32(0),
                            questionType_id = reader.GetInt32(1),
                            question_sentence = reader.GetString(2),
                            word_id = reader.GetInt32(3)
                        }); ;
                    }
                    return question;
                }
                return null;
            } 
            catch(Exception ex)
            {
                throw;
            }
            
        }
        public bool UpdateWordLevelInNotebook(int word_id)
        {
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "update Notebook_word set level_id=level_id+1 where word_id=@word_id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@word_id", word_id);
                var reader = sqlCommand.ExecuteNonQuery();
                if (reader > 0)
                {
                    return true;
                }
            }
            catch (System.Exception ex)
            {
            }
            return false;
        }
        
    }
}
