﻿using System.Data.SqlClient;
using static Group3.DBContext;
namespace Group3.Models.DAO
{
    public class CourseDAO
    {
        public Course GetCourseByID(int? course_id)
        {
            try
            {
                Course course = new Course();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from Course where course_id = @course_id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@course_id", course_id);
                using var reader = sqlCommand.ExecuteReader();
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        course.course_id = reader.GetInt32(0);
                        course.course_name = reader.GetString(1);
                        course.course_total_chapters = reader.GetInt32(2);
                        course.course_create_data = reader.GetDateTime(3);
                        course.teacher_id = reader.GetInt32(4);
                        course.course_image = reader.GetString(5);
                        course.participant=reader.GetInt32(6);
                    }
                    return course;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return null;
        }
        public List<Course> GetCourseByIDandName(string name)
        {
            try
            {
                List<Course> courseList = new List<Course>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                //select * from Course where course_id = @course_id OR course_name LIKE '%@course_name%'
                string query = "select * from Course where course_name LIKE @course_name";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@course_name", "%"+name+"%");
                using var reader = sqlCommand.ExecuteReader();
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        courseList.Add(new Course{
                            course_id = reader.GetInt32(0),
                            course_name = reader.GetString(1),
                            course_total_chapters = reader.GetInt32(2),
                            course_create_data = reader.GetDateTime(3),
                            teacher_id = reader.GetInt32(4),
                            course_image = reader.GetString(5),
                            participant=reader.GetInt32(6)
                        });
                    }
                    return courseList;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return null;
        }

        public List<Course> GetCourses()
        {
            try
            {
                List<Course> CourseList= new List<Course>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from Course ";
                sqlCommand.CommandText = query;
                using var reader = sqlCommand.ExecuteReader();
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        CourseList.Add(new Course
                        {
                            course_id = reader.GetInt32(0),
                            course_name = reader.GetString(1),
                            course_total_chapters = reader.GetInt32(2),
                            course_create_data = reader.GetDateTime(3),
                            teacher_id = reader.GetInt32(4),
                            course_image = reader.GetString(5),
                            participant=reader.GetInt32(6)
                        });
                    }
                    return CourseList;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return null;
        }
        public int totalCourse(){
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT COUNT(*) FROM dbo.Course";
                sqlCommand.CommandText = query;
                var reader= sqlCommand.ExecuteScalar();
                if(reader!=null){
                    return (int) reader;
                }
            }
            catch (System.Exception ex)
            {
            }
            return 0;
        }
        public bool InsertCourse(string name,int totalChapter,int teacherID,string url, int participant){
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "INSERT INTO dbo.Course ( course_name, course_total_chapters, course_create_date, teacher_id, course_image, participant ) VALUES ( @name,@total, GETDATE(), @teacherID, @url, @participant )";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@name", name);
                sqlCommand.Parameters.AddWithValue("@total", totalChapter);
                sqlCommand.Parameters.AddWithValue("@teacherID", teacherID);
                sqlCommand.Parameters.AddWithValue("@url", url);
                sqlCommand.Parameters.AddWithValue("@participant", participant);
                var reader= sqlCommand.ExecuteNonQuery();
                if(reader!=null){
                    return true;
                }
            }
            catch (System.Exception ex)
            {
            }
            return false;
        }
        public int getIDCourseLaster(){
            try
            {
                //SELECT MAX(course_id) FROM dbo.Course
                int count=-1;
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT MAX(course_id) FROM dbo.Course";
                sqlCommand.CommandText = query;
                using var reader = sqlCommand.ExecuteReader();
                if (reader != null)
                {
                    while(reader.Read()){
                        count=reader.GetInt32(0);
                    }
                }
                return count;
            }
            catch (System.Exception ex)
            {}
            return -1;
        }
        public bool UploadCourse(string name,string url,int id){
            //UPDATE dbo.Course SET course_name='' ,course_image='' WHERE course_id=?
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "UPDATE dbo.Course SET course_name=@name ,course_image=@url WHERE course_id=@id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@name", name);
                sqlCommand.Parameters.AddWithValue("@url", url);
                sqlCommand.Parameters.AddWithValue("@id", id);
                var reader = sqlCommand.ExecuteNonQuery();
                if(reader > 0){
                    return true;
                }
            }
            catch (System.Exception ex)
            {
                 // TODO
            }
            return false;
        }
        public bool DeleteCourse(int id){
            //DELETE FROM dbo.Course WHERE course_id=
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "DELETE FROM dbo.Course WHERE course_id=@id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@id", id);
                var reader = sqlCommand.ExecuteNonQuery();
                if(reader > 0){
                    return true;
                }
            }
            catch (System.Exception ex)
            {
                 // TODO
            }
            return false;
        }
        public bool DeleteEmptyCourse(){
            //DELETE FROM dbo.Course WHERE course_name=''
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "DELETE FROM dbo.Course WHERE course_name=''";
                sqlCommand.CommandText = query;
                using var reader = sqlCommand.ExecuteReader();
                if(reader !=null){
                    return true;
                }
            }
            catch (System.Exception ex)
            {
            }
            return false;
        }
        public Course CheckDuplicateCourse(string name){
            //DELETE FROM dbo.Course WHERE course_name=''
            try
            {
                Course course=  new Course();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT * FROM dbo.Course WHERE course_name=@name";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@name", name);
                using var reader = sqlCommand.ExecuteReader();
                if(reader.HasRows){
                    while(reader.Read()){
                        course.course_id = reader.GetInt32(0);
                        course.course_name = reader.GetString(1);
                        course.course_total_chapters = reader.GetInt32(2);
                        course.course_create_data = reader.GetDateTime(3);
                        course.teacher_id = reader.GetInt32(4);
                        course.course_image = reader.GetString(5);
                        course.participant=reader.GetInt32(6);
                    }
                    return course;
                }
            }
            catch (System.Exception ex)
            {
            }
            return null;
        }
        public bool UpdateTotalChapter(int idCourse,int total){
            //UPDATE dbo.Course SET course_name='' ,course_image='' WHERE course_id=?
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "UPDATE dbo.Course SET course_total_chapters=@total WHERE course_id=@idCourse";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@total",total );
                sqlCommand.Parameters.AddWithValue("@idCourse", idCourse);
                var reader = sqlCommand.ExecuteNonQuery();
                if(reader > 0){
                    return true;
                }
            }
            catch (System.Exception ex)
            {
                 // TODO
            }
            return false;
        }
        //select COUNT(*) from dbo.ChapterCourse where course_id = @course_id
        public int totalCoursebyCourseID(int courseID){
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select COUNT(*) from dbo.ChapterCourse where course_id = @course_id";
                sqlCommand.Parameters.AddWithValue("@course_id", courseID);
                sqlCommand.CommandText = query;
                var reader= sqlCommand.ExecuteScalar();
                if(reader!=null){
                    return (int) reader;
                }
            }
            catch (System.Exception ex)
            {
            }
            return 0;
        }
        
        public int GetTeacherIDByChapterID(int chapterID){
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select teacher_id from ChapterCourse cc join Course c on cc.course_id = c.course_id where cc.chapter_id = @chapter_id";
                sqlCommand.Parameters.AddWithValue("@chapter_id", chapterID);
                sqlCommand.CommandText = query;
                var reader= sqlCommand.ExecuteScalar();
                if(reader!=null){
                    return (int) reader;
                }
            }
            catch (System.Exception ex)
            {
            }
            return 0;
        }
    }
}
