using System.Data.SqlClient;
using static Group3.DBContext;
namespace Group3.Models.DAO
{
    public class ExamDAO
    {
        public Exam GetExamById(int examId)
        {
            try
            {
                Exam exam = new Exam();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from Exam where exam_id = @exam_id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@exam_id", examId);
                using var reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        exam.exam_id = reader.GetInt32(0);
                        exam.exam_total_questions = reader.GetInt32(1);
                        exam.exam_point = reader.GetDouble(2);
                        exam.exam_name = reader.GetString(3);
                        exam.exam_times = reader.GetString(4);
                        exam.teacher_id = reader.GetInt32(5);
                        exam.exam_description = reader.GetString(6);
                    }
                    return exam;
                }
            }
            catch (System.Exception ex)
            {
            }
            return null;
        }

        public List<Exam> getListExam()
        {
            try
            {
                List<Exam> listExam = new List<Exam>();
                using SqlCommand sqlComannd = new SqlCommand();
                sqlComannd.Connection = SQL;
                string query = "SELECT * FROM dbo.[Exam]";
                sqlComannd.CommandText = query;
                using var reader = sqlComannd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {

                        listExam.Add(new Exam
                        {
                            exam_id = reader.GetInt32(0),
                            exam_total_questions = reader.GetInt32(1),
                            exam_point = reader.GetDouble(2),
                            exam_name = reader.GetString(3),
                            exam_times = reader.GetString(4),
                            teacher_id = reader.GetInt32(5),
                            exam_description = reader.GetString(6)
                        });
                    }
                    return listExam;
                }
                reader.Close();
            }
            catch (System.Exception ex)
            {
            }
            return null;
        }

        public List<Questions> GetQuestionsByExamID(int exam_id)
        {
            try
            {
                List<Questions> questionlist = new List<Questions>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from Questions q inner join QuestionExam qe on qe.question_id = q.question_id inner join Words w on q.word_id = w.word_id where qe.exam_id = @exam_id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@exam_id", exam_id);
                using var reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        questionlist.Add(new Questions
                        {
                            question_id = reader.GetInt32(0),
                            questionType_id = reader.GetInt32(1),
                            question_sentence = reader.GetString(2),
                            word_id = reader.GetInt32(3)
                        });
                    }
                    return questionlist;
                }
                return null;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public bool DeleteExam(int id){
            //DELETE FROM dbo.Course WHERE course_id=
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "DELETE FROM dbo.Exam WHERE exam_id=@id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@id", id);
                var reader = sqlCommand.ExecuteNonQuery();
                if(reader > 0){
                    return true;
                }
            }
            catch (System.Exception ex)
            {
                 // TODO
            }
            return false;
        }
        public bool DelteExamTeacher(int id){
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "DELETE FROM dbo.ExamTeacher WHERE exam_id=@id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@id", id);
                var reader = sqlCommand.ExecuteNonQuery();
                if(reader !=0){
                    return true;
                }
            }
            catch (System.Exception ex)
            {
            }
            return false;
        }
        public bool DeleteExamQuestions(int id){
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "DELETE FROM dbo.QuestionExam WHERE exam_id=@id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@id", id);
                var reader = sqlCommand.ExecuteNonQuery();
                if(reader!=0){
                    return true;
                }
            }
            catch (System.Exception ex)
            {
            }
            return false;
        }
    }
}
