﻿using System.Data.SqlClient;
using static Group3.DBContext;
namespace Group3.Models.DAO
{
    public class NotebookDAO
    {
        public void AddWordsToNotebook(List<Word> wordlist, string notebook_id)
        {
            using SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.Connection = SQL;
            foreach (Word word in wordlist)
            {
                string query = "insert into Notebook_word(notebook_id, level_id, word_id) values(@notebook_id, 1, @word_id)";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@notebook_id", notebook_id);
                sqlCommand.Parameters.AddWithValue("@word_id", word.word_id);
                sqlCommand.ExecuteNonQuery();
            }
        }

        public Notebook GetNotebookFromUserID(int user_id)
        {
            Notebook notebook = new Notebook();
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from NoteBookUser nbu where nbu.user_id = @user_id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@user_id", user_id);
                using var reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        notebook.user_id = user_id;
                        notebook.notebook_id = reader.GetInt32(1);
                    }
                }
                reader.Close();
                return notebook;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public int GetNumberOfWordFromNotebookWithLevel(Notebook notebook, int level)
        {
            int wordCount = 0;
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select COUNT(nbw.word_id) from Notebook_word nbw where nbw.notebook_id = @notebook_id and nbw.level_id = @level";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@notebook_id", notebook.notebook_id);
                sqlCommand.Parameters.AddWithValue("@level", level);
                using var reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        wordCount = reader.GetInt32(0);                       
                    }
                    return wordCount;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return wordCount;
        }

        public List<Word> GetNotebookWordsWithLevel(Notebook notebook, int level)
        {
            List<Word> wordlist = new List<Word>();
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from Notebook_word nbw join Words w on nbw.word_id = w.word_id where nbw.notebook_id = @notebook_id and nbw.level_id = @level";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@notebook_id", notebook.notebook_id);
                sqlCommand.Parameters.AddWithValue("@level", level);
                using var reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        wordlist.Add(new Word{
                            word_id = reader.GetInt32(3),
                            word_face = reader.GetString(4),
                            word_nghia = reader.GetString(5),
                            word_IPA = reader.GetString(6),
                            wordType_id = reader.GetString(7),
                            word_phrase = reader.GetString(8),
                            teacher_id = reader.GetInt32(9),
                            word_image = reader.GetString(10)
                        });
                    }
                    return wordlist;
                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
            return null;
        }

        public void AddWordsIntoNotebook(List<Word> wordlist, Notebook notebook)
        {
            try
            {
                foreach (Word word in wordlist)
                {
                    using SqlCommand sqlCommand = new SqlCommand();
                    sqlCommand.Connection = SQL;
                    string query = "insert into Notebook_word(notebook_id, level_id, word_id) values (@notebook_id, @level_id, @word_id)";
                    sqlCommand.CommandText = query;
                    sqlCommand.Parameters.AddWithValue("@notebook_id", notebook.notebook_id);
                    sqlCommand.Parameters.AddWithValue("@level_id", 1);
                    sqlCommand.Parameters.AddWithValue("@word_id", word.word_id);
                   
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                
                throw;
            }
        }
    }
}
