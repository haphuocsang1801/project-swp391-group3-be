﻿using System.Data.SqlClient;
using static Group3.DBContext;
namespace Group3.Models.DAO
{
    public class WordDAO
    {
        public Word GetWord(int WordId)
        {
            try
            {
                Word word = new Word();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from[Words] where word_id = @word_id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@word_id", WordId);
                using var reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        word.word_id = reader.GetInt32(0);
                        word.word_face = reader.GetString(1);
                        word.word_nghia = reader.GetString(2);
                        word.word_IPA = reader.GetString(3);
                        word.wordType_id = reader.GetString(4);
                        word.word_phrase = reader.GetString(5);
                        word.teacher_id = reader.GetInt32(6);
                        word.word_image = reader.GetString(7);
                    }
                    return word;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return null;
        }
        
        public List<Word> GetWordsByNameInLevelInNotebook(Notebook notebook, string name, string level)
        {
            try
            {
                List<Word> wordlist = new List<Word>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from Notebook_word nbw join Words w on nbw.word_id = w.word_id where nbw.notebook_id = @notebook_id and nbw.level_id = @level and w.word_face like @word_face";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@word_face", "%"+name+"%");
                sqlCommand.Parameters.AddWithValue("@notebook_id", notebook.notebook_id);
                sqlCommand.Parameters.AddWithValue("@level", level);
                using var reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        wordlist.Add(new Word
                        {
                            word_id = reader.GetInt32(3),
                            word_face = reader.GetString(4),
                            word_nghia = reader.GetString(5),
                            word_IPA = reader.GetString(6),
                            wordType_id = reader.GetString(7),
                            word_phrase = reader.GetString(8),
                            teacher_id = reader.GetInt32(9),
                            word_image = reader.GetString(10)
                        });
                    }
                    return wordlist;
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return null;
        }
        public List<Word> GetWordsByChapterID(int chapter_id)
        {
            try
            {
                List<Word> wordlist = new List<Word>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from Words w join ChapterWord cw on w.word_id = cw.word_id where cw.chapter_id = @chapter_id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@chapter_id", chapter_id);
                using var reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        wordlist.Add(new Word
                        {
                            word_id = reader.GetInt32(0),
                            word_face = reader.GetString(1),
                            word_nghia = reader.GetString(2),
                            word_IPA = reader.GetString(3),
                            wordType_id = reader.GetString(4),
                            word_phrase = reader.GetString(5),
                            teacher_id = reader.GetInt32(6),
                            word_image = reader.GetString(7),
                        });
                    }
                    return wordlist;
                }
                return null;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public List<WordType> GetWordTypes()
        {
            try
            {
                List<WordType> WordTypelist = new List<WordType>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT * FROM dbo.WordType";
                sqlCommand.CommandText = query;
                using var reader = sqlCommand.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        WordTypelist.Add(new WordType{
                            wordtype_id=reader.GetString(0),
                            wordtype_name=reader.GetString(1)
                        });
                    }
                    return WordTypelist;
                }
                return null;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public bool InsertWord(string word_face,string nghia,string ipa,string? type_id,string ex,int teacher_id,string fileImage){
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "INSERT dbo.Words ( word_face, word_nghia, word_IPA, wordType_id, word_phrase, teacher_id, word_image ) VALUES ( @word_face, @nghia, @ipa, @type_id, @ex, @teacher_id, @fileImage)";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@word_face", word_face);
                sqlCommand.Parameters.AddWithValue("@nghia", nghia);
                sqlCommand.Parameters.AddWithValue("@ipa", ipa);
                sqlCommand.Parameters.AddWithValue("@type_id", type_id);
                sqlCommand.Parameters.AddWithValue("@ex", ex);
                sqlCommand.Parameters.AddWithValue("@teacher_id", teacher_id);
                sqlCommand.Parameters.AddWithValue("@fileImage", fileImage);
                var reader= sqlCommand.ExecuteNonQuery();
                if(reader!=null){
                    return true;
                }
            }
            catch (Exception ex1)
            {
            }
            return false;
        }
        public bool UpdateWord(string word_face,string nghia,string ipa,string? type_id,string ex,string fileImage,int word_id){
            try
            {
                string query="";
                if(fileImage!=null){
                    query = "UPDATE dbo.Words SET word_face=@word_face ,word_nghia=@nghia ,word_IPA=@ipa ,wordType_id=@type_id ,word_phrase=@ex , word_image=@fileImage WHERE word_id=@word_id";
                }
                else{
                    query = "UPDATE dbo.Words SET word_face=@word_face ,word_nghia=@nghia ,word_IPA=@ipa ,wordType_id=@type_id ,word_phrase=@ex WHERE word_id=@word_id";
                }
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@word_face", word_face);
                sqlCommand.Parameters.AddWithValue("@nghia", nghia);
                sqlCommand.Parameters.AddWithValue("@ipa", ipa);
                sqlCommand.Parameters.AddWithValue("@type_id", type_id);
                sqlCommand.Parameters.AddWithValue("@ex", ex);
                if(fileImage!=null)
                    sqlCommand.Parameters.AddWithValue("@fileImage", fileImage);
                sqlCommand.Parameters.AddWithValue("@word_id", word_id);
                var reader= sqlCommand.ExecuteNonQuery();
                if(reader!=null){
                    return true;
                }
            }
            catch (Exception ex1)
            {
            }
            return false;
        }
        public bool InsertWordChapter(int chapter_id,int word_id){
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "INSERT dbo.ChapterWord ( chapter_id, word_id ) VALUES (@chapter_id, @word_id)";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@chapter_id", chapter_id);
                sqlCommand.Parameters.AddWithValue("@word_id", word_id);
                var reader= sqlCommand.ExecuteNonQuery();
                if(reader!=null){
                    return true;
                }
            }
            catch (Exception ex1)
            {
            }
            return false;
        }
        public int getIDWordLaster(){
            try
            {
                //SELECT MAX(course_id) FROM dbo.Course
                int count=-1;
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT MAX(word_id) FROM dbo.Words ";
                sqlCommand.CommandText = query;
                using var reader = sqlCommand.ExecuteReader();
                if (reader != null)
                {
                    while(reader.Read()){
                        count=reader.GetInt32(0);
                    }
                }
                return count;
            }
            catch (System.Exception ex)
            {}
            return -1;
        }
        public bool DeleteQuesiton(int word_id){
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "DELETE dbo.Words WHERE word_id=@id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@id", word_id);
                var reader = sqlCommand.ExecuteNonQuery();
                if(reader > 0){
                    return true;
                }
            }
            catch (System.Exception ex)
            {}
            return false;
        }
        public bool DeleteChapterWord(int word_id){
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "DELETE dbo.ChapterWord WHERE word_id=@id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@id", word_id);
                var reader = sqlCommand.ExecuteNonQuery();
                if(reader > 0){
                    return true;
                }
            }
            catch (System.Exception ex)
            {}
            return false;
        }
    }
}
