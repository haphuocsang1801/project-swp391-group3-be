namespace Group3.Models
{
    public class Questions
    {
        public int question_id { set; get; }
        public int questionType_id { set; get; }
        public string question_sentence { set; get; }
        public int word_id { set; get; }

    }
}