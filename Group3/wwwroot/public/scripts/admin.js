"use strict";
function _toConsumableArray(a) {
  if (Array.isArray(a)) {
    for (var e = 0, r = Array(a.length); e < a.length; e++) r[e] = a[e];
    return r;
  }
  return Array.from(a);
}
var myChartSale = document.querySelector("#myChart");
console.log(myChartSale),
  myChartSale &&
    handleAjax("post", "/Report/handleChartByWeekly", hanleChartByWeekly);
var chart = void 0;
function handleDataChart(a) {
  a = a.textContent.toLowerCase();
  "weekly" == a
    ? handleAjax("post", "/Report/handleChartByWeekly", hanleChartByWeekly)
    : "monthly" == a &&
      handleAjax("post", "/Report/hanleChartByMonthLy", hanleChartByMonthLy);
}
function hanleChartByMonthLy(a) {
  for (var e = [], r = 1; r <= a.length; r++) e.push(r);
  var t = Math.max.apply(Math, _toConsumableArray(a));
  a.push(t + 10),
    null != chart && chart.destroy(),
    (chart = handleChart(myChartSale, a, e));
}
function hanleChartByWeekly(a) {
  var e = Math.max.apply(Math, _toConsumableArray(a));
  a.push(e + 5),
    null != chart && chart.destroy(),
    (chart = handleChart(myChartSale, a, [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday",
    ]));
}
function handleChart(a, e, r) {
  return new Chart(a, {
    type: "line",
    data: {
      labels: r,
      datasets: [
        {
          label: "Revenue",
          backgroundColor: "rgb(255, 99, 132)",
          borderColor: "rgb(255, 99, 132)",
          data: e,
          tension: 0.1,
        },
      ],
    },
    options: {},
  });
}
var myChartQuiz = document.querySelector("#myChartQuizzes");
function handleMutipleChart(a) {
  new Chart(a, {
    type: "scatter",
    data: {
      labels: ["January", "February", "March", "April", "May", "June"],
      datasets: [
        {
          type: "line",
          label: "Not Pass",
          backgroundColor: "rgb(255, 99, 132)",
          borderColor: "rgb(255, 99, 132)",
          data: [0, 10, 5, 2, 20, 100, 200],
        },
        {
          type: "line",
          label: "Pass",
          backgroundColor: "rgb(117, 206, 85)",
          borderColor: "rgb(117, 206, 85)",
          data: [0, 20, 15, 31, 21, 180, 200],
        },
      ],
    },
    options: { scales: { y: { beginAtZero: !0 } } },
  });
}
myChartQuiz && handleMutipleChart(myChartQuiz);
var featureItems = document.querySelectorAll(".admin-feature-item ");
[].concat(_toConsumableArray(featureItems)).forEach(function (e) {
  e.addEventListener("click", function (a) {
    [].concat(_toConsumableArray(featureItems)).forEach(function (a) {
      a.classList.remove("feature-click");
    }),
      e.classList.add("feature-click");
  });
});
