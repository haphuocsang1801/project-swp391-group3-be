let h = 0;
let m = 0;
let s = 15;

var timeout = null;
function start() {
    if (m === null) {
        m = parseInt(document.getElementById("m").value);
        s = parseInt(document.getElementById("s").value);
    }

    if (s === -1) {
        m -= 1;
        s = 59;
    }

    if (m == -1) {
        clearTimeout(timeout);
        return false;
    }

    document.getElementById("m").innerText = m.toString();
    document.getElementById("s").innerText = s.toString();

    timeout = setTimeout(function () {
        s--;
        start();
    }, 1000);
}
window.onload = start;
