const openModalButtons = document.querySelector(".course-select");
const closeModalButtons = document.querySelector(".close-button");
const modal = document.querySelector(".courseselection");
const overlay = document.querySelector(".courseselectOverlay");
const userprofile = document.querySelector(".icons-user");
openModalButtons.addEventListener("click", (e) => {
    if (modal == null) return;
    modal.classList.add("active");
    overlay.classList.add("active");
});

closeModalButtons.addEventListener("click", (e) => {
    if (modal == null) return;
    modal.classList.remove("active");
    overlay.classList.remove("active");
});

document.body.addEventListener("click", (e) => {
    if (e.target.matches(".courseselectOverlay")) {
        overlay.classList.remove("active");
        modal.classList.remove("active");
    }
});

window.onload = () => {
    const tab_switchers = document.querySelectorAll("[data-switcher]");
    const level_switchers = document.querySelectorAll("[data-switcher-2]");
    for (let i = 0; i < tab_switchers.length; i++) {
        const tab_switcher = tab_switchers[i];
        const page_id = tab_switcher.dataset.tab;
        tab_switcher.addEventListener("click", () => {
            document
                .querySelector(".icons-icon.is-active")
                .classList.remove("is-active");
            tab_switcher.classList.add("is-active");
            switchPage(page_id);
        });
    }
    for (let i = 0; i < level_switchers.length; i++) {
        const level_switcher = level_switchers[i];
        const level = level_switcher.dataset.level;
        level_switcher.addEventListener("click", () => {
            document
                .querySelector(".level-bar-level.is-active")
                .classList.remove("is-active");
            level_switcher.classList.add("is-active");

            switchLevel(level);
        });
    }

    document.querySelector(".wordlists-level:first-child").classList.add("is-active");
};

function switchPage(page_id) {
    const currentPage = document.querySelector(".pages .page.is-active");
    currentPage.classList.remove("is-active");

    const nextPage = document.querySelector(
        `.pages .page[data-page="${page_id}"]`
    );
    nextPage.classList.add("is-active");
}

function switchLevel(level) {
    const currentPage = document.querySelector(".wordlists-level.is-active");
    currentPage.classList.remove("is-active");

    const nextPage = document.querySelector(
        `.wordlists-level[data-word-level="${level}"]`
    );
    nextPage.classList.add("is-active");
}

userprofile.addEventListener('click', () =>{
    // window.location.href = ""
})

$(document).ready(() => {
    $("#Search").keyup(() => {
      var SearchValue = $("#Search").val();
      var SearchLevel = $(".level-bar-level.is-active[data-level]").attr("data-level");
      var setData = $(".DataSearching.wordlists-level.is-active")
      setData.html("");
      $.ajax({
          type: "post",
          url: "/LearningPage/GetSearchData?SearchLevel=" + SearchLevel + "&SearchValue=" + SearchValue,
          contentType: "html",
          success: function(result) {
            //   console.log(result);
              if (result == null)
              {
                setData.append("<div class='empty'>No result</div>");
              } else if (typeof(result) != 'object'){
                result = $.parseJSON(result);
              }else{
                result.forEach((word) => {
                    if(word.wordType_id == "1"){
                        var Data =  `<div class="wordrow"> 
                                    <div class="wordrow-vocab">${word.word_face} </div> 
                                    <div class="wordrow-ipa">${word.word_IPA}</div> 
                                    <div class="wordrow-wordtype">n</div> 
                                    <div class="wordrow-defi">${word.word_nghia}</div> 
                                </div>`;
                        } else if(word.wordType_id =="2") {
                        var Data =  `<div class="wordrow"> 
                                    <div class="wordrow-vocab">${word.word_face} </div> 
                                    <div class="wordrow-ipa">${word.word_IPA}</div> 
                                    <div class="wordrow-wordtype">adj</div>
                                    <div class="wordrow-defi">${word.word_nghia}</div> 
                                </div>`;
                        } else if(word.wordType_id == "3"){
                        var Data =  `<div class="wordrow"> 
                                    <div class="wordrow-vocab">${word.word_face} </div> 
                                    <div class="wordrow-ipa">${word.word_IPA}</div> 
                                    <div class="wordrow-wordtype">v</div> 
                                    <div class="wordrow-defi">${word.word_nghia}</div> 
                                </div>`;
                        }
                        setData.append(Data);
                });   
              }
            //   window.location = "/LearningPage";
          }
      })
    })
  })