const myChartSale = document.querySelector("#myChart");
console.log(myChartSale);
if (myChartSale) {
    handleAjax("post", "/Report/handleChartByWeekly", hanleChartByWeekly);
}
let chart;

function handleDataChart(event) {
    const selected = event.textContent.toLowerCase();
    if (selected == "weekly") {
        handleAjax("post", "/Report/handleChartByWeekly", hanleChartByWeekly);
    } else if (selected == "monthly") {
        handleAjax("post", "/Report/hanleChartByMonthLy", hanleChartByMonthLy);
    }
}
function hanleChartByMonthLy(result) {
    let labels = [];
    for (let index = 1; index <= result.length; index++) {
        labels.push(index);
    }
    let maxValue = Math.max(...result);
    result.push(maxValue + 10);
    if (chart != undefined) {
        chart.destroy();
    }
    chart = handleChart(myChartSale, result, labels);
}
function hanleChartByWeekly(result) {
    let labels = [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday",
    ];
    let maxValue = Math.max(...result);
    result.push(maxValue + 5);
    if (chart != undefined) {
        chart.destroy();
    }
    chart = handleChart(myChartSale, result, labels);
}
function handleChart(myChartSale, result, labels) {
    const data = {
        labels: labels,
        datasets: [
            {
                label: "Revenue",
                backgroundColor: "rgb(255, 99, 132)",
                borderColor: "rgb(255, 99, 132)",
                data: result,
                tension: 0.1,
            },
        ],
    };
    const config = {
        type: "line",
        data: data,
        options: {},
    };
    return new Chart(myChartSale, config);
}

const myChartQuiz = document.querySelector("#myChartQuizzes");
if (myChartQuiz) {
    handleMutipleChart(myChartQuiz);
}
function handleMutipleChart(chart) {
    const labels = ["January", "February", "March", "April", "May", "June"];
    const data = {
        labels: labels,
        datasets: [
            {
                type: "line",
                label: "Not Pass",
                backgroundColor: "rgb(255, 99, 132)",
                borderColor: "rgb(255, 99, 132)",
                data: [0, 10, 5, 2, 20, 100, 200],
            },
            {
                type: "line",
                label: "Pass",
                backgroundColor: "rgb(117, 206, 85)",
                borderColor: "rgb(117, 206, 85)",
                data: [0, 20, 15, 31, 21, 180, 200],
            },
        ],
    };
    const config = {
        type: "scatter",
        data: data,
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                },
            },
        },
    };
    const myChart = new Chart(chart, config);
}

//----------------------------------------------------------------------------

//feature-click
const featureItems = document.querySelectorAll(".admin-feature-item ");
[...featureItems].forEach((item) => {
    item.addEventListener("click", function (e) {
        [...featureItems].forEach((elem) => {
            elem.classList.remove("feature-click");
        });
        item.classList.add("feature-click");
    });
});
