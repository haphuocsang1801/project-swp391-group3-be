var synth = window.speechSynthesis;
var utterance = new SpeechSynthesisUtterance("Hello World");
if ("speechSynthesis" in window) {
    var synthesis = window.speechSynthesis;
    // Get the first `en` language voice in the list
    var voice = synthesis.getVoices().filter(function (voice) {
        return voice.lang === "en";
    })[0];

    // Create an utterance object
    var utterance = new SpeechSynthesisUtterance("Hello World");

    // Set utterance properties
    utterance.voice = voice;
    utterance.pitch = 2;
    utterance.rate = 1.25;
    utterance.volume = 0.6;
} else {
    console.log("not SUpported");
}
const wordFlipCards = document.querySelectorAll(
    ".div-learn-content[data-word-number]"
);
const button1= document.querySelectorAll(".button");
const continueBtn = document.querySelector(".test");
const textBoxes = document.querySelectorAll(".textbox");
const fillWordTextBoxes = document.querySelectorAll(".fill-in-the-word");
const popup = document.querySelector(".div-answer");
const listenBtns = document.querySelectorAll(".div-audio-icon");
const contentDivs = document.querySelectorAll(".div-reviewing-content");
const progressBar = document.querySelector(".progress-bar");
var answer = false;
var change = Math.floor(600 / wordFlipCards.length);
window.onload = () => {
    document
        .querySelector(".div-reviewing-content:first-child")
        .classList.add("active");
};
var incorrectQuestion;
continueBtn.addEventListener('click', (e) => {
    
    var currentContent = document.querySelector(".div-reviewing-content.active");
    let popup = document.querySelector(
        `.div-answer[data-answer="${currentContent.getAttribute(
            "data-answer"
        )}"]`
    );
    if (
        ContinueBtnState() &&
        document
            .querySelector(".div-reviewing-content.active")
            .hasAttribute("data-word-number")
    ) {
        turnOffButton();
        var nextContent = currentContent.nextElementSibling;
        nextContent.classList.add("active");
        currentContent.classList.remove("active");
        //Progress bar
        if (currentContent.getAttribute("data-word-number") == 0) {
            progressBar.setAttribute("style", `width:${change}px`);
        } else {
            progressBar.setAttribute("style", `width:${change}px`);
        }
    } else if (ContinueBtnState() && PopupState(popup)) {
        //Popup present continue
        turnOffButton();
        let nextContent;
        if (currentContent.querySelector(".textbox") != null) {
            currentContent.querySelector(".textbox").value = "";
        }
        if (currentContent.querySelector(".fill-in-the-word") != null) {
            currentContent.querySelector(".fill-in-the-word").value = "";
        }
        if (
            currentContent.nextElementSibling.classList.contains("div-answer")
        ) {
            nextContent = currentContent.nextElementSibling.nextElementSibling;
        } else {
            nextContent = currentContent.nextElementSibling;
        }
        if (
            document
                .querySelectorAll(".div-reviewing-content")
                [contentDivs.length - 1].classList.contains("active")
        ) {
            //Done learning
            window.location.href = "/LearningPage/AddWordAndChapter";
        }
        nextContent.classList.add("active");
        currentContent.classList.remove("active");
        if (popup.classList.contains("correct")) {
            popup.classList.remove("correct");
        } else {
            popup.classList.remove("incorrect");
            insertAfter(
                document.querySelectorAll(".div-reviewing-content")[
                    contentDivs.length - 1
                ],
                incorrectQuestion
            );
        }
    } else if (ContinueBtnState() && !PopupState(popup)) {
        //Bring up popup if not present
        if (answer) {
            popup.classList.add("correct");
        } else popup.classList.add("incorrect");
        incorrectQuestion = document.querySelector(".div-reviewing-content.active");
        answer = false;
    }

});

textBoxes.forEach((textbox) => {
    textbox.addEventListener("keyup", () => {
        if (textbox.value != "") {
            turnOnButton();
        } else turnOffButton();
        answer = checkTextBoxAnswer(textbox);
    });
});

fillWordTextBoxes.forEach((fwtextbox) => {
    fwtextbox.addEventListener("keyup", () => {
        if (fwtextbox.value != "") {
            turnOnButton();
        } else {
            turnOffButton;
        }
        answer = checkTextBoxAnswer(fwtextbox);
    });
});
for (let i = 0; i < button1.length; i++) {
    const element = button1[i];
    element.addEventListener('click', (e) =>{
        console.log("hello");
        turnOnButton();
    });
}
listenBtns.forEach((btn) => {
    btn.addEventListener("click", () => {
        const listen_answer = document
            .querySelector(".div-reviewing-content.active")
            .getAttribute("data-answer");
        utterance.text = listen_answer;
        synthesis.speak(utterance);
    });
});

function insertAfter(referenceNode, newNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}
function checkTextBoxAnswer(textbox) {
    return (
        textbox.value.toLowerCase() ==
        textbox.parentElement.getAttribute("data-answer").toLowerCase()
    );
}

function PopupState(popup) {
    return (
        popup.classList.contains("correct") ||
        popup.classList.contains("incorrect")
    );
}

function ContinueBtnState() {
    return continueBtn.classList.contains("active");
}

function turnOffButton() {
    continueBtn.classList.remove("active");
}

function turnOnButton() {
    continueBtn.classList.add("active");
}
