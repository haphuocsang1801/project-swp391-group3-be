const valueSearchFeedback = document.querySelector("#valueSearchTeacher");
const valueSearchAdmin = document.querySelector("#valueSearchAdmin");
if (valueSearchAdmin) {
    valueSearchAdmin.addEventListener("keyup", function (e) {
        let value = e.target.value;
        handleAjax(
            "post",
            "/Feedback/SearchFeedback?value=" + value,
            handeleSearchFeedback
        );
    });
}
if (valueSearchFeedback) {
    valueSearchFeedback.addEventListener("keyup", function (e) {
        let value = e.target.value;
        handleAjax(
            "post",
            "/Feedback/SearchFeedbackByIDTeacher?value=" + value,
            handeleSearchFeedback
        );
    });
}
function handeleSearchFeedback(result) {
    console.log(result);
    const resultSearchFeedback = document.querySelector(
        ".resultSearchFeedback"
    );
    resultSearchFeedback.innerHTML = "";
    result.feedbacksList.forEach((item) => {
        result.userList.forEach((element) => {
            if (item.user_id == element.userID) {
                let time = item.feedback_time.split("T");
                let templte = `
                <tr>
                    <td>
                        <div class="admin-table-number">${
                            item.feedback_id
                        }</div>
                    </td>
                    <td>
                        <div class="admin-table-content">
                            ${item.feedback_content}
                        </div>
                    </td>
                    <td>
                        <div class="admin-table-name" style="justify-content: center;"> <img class="admin-table-image" src="${
                            element.urlAvatar
                        }" alt="">
                        <p class="admin-table-name">${element.fullName}</p>
                        </div>
                    </td>
                    <td>
                        <div class="admin-table-type">${time[0]}</div>
                    </td>
                    <td>
                    ${
                        item.status
                            ? ` <p class="admin-table-action admin-table-action--green">resolve</p>`
                            : `<p class="admin-table-action admin-table-action--red" >unresolve</p>`
                    }
                    </td>
                    <td> <a class="admin-table-action admin-table-action--red" asp-action="UpdateStatus" asp-controlle="Feedback" asp-route-status="${
                        item.status
                    }" asp-route-id="${
                    item.feedback_id
                }" asp-route-role="2"><i class="far fa-trash"></i>
                    </a>
                    </td>
                </tr>
            `;
                resultSearchFeedback.insertAdjacentHTML("beforeend", templte);
            }
        });
    });
}
