$(document).ready(function () {
    $("#SearchCourse").keyup(function () {
        let SearchValue = $("#SearchCourse").val();
        let SetData = $("#DataSearching");
        SetData.html("");
        $.ajax({
            type: "post",
            url: "GetCourseAdmin?SearchValue=" + SearchValue,
            contentType: "html",
            success: function (result) {
                if (result.length == 0) {
                    SetData.append("<tr><td>Not Found</td></tr>");
                } else {
                    result.courses.forEach((item) => {
                        let author;
                        result.users.forEach((elem) => {
                            if (elem.userID == item.teacher_id) {
                                author = elem.fullName;
                            }
                        });
                        let dateText = item.course_create_data;
                        let dateOnly = dateText.split("T");
                        let Data = `
                        <tr>
                            <td>
                                <div class="admin-table-number">${item.course_id}</div>
                            </td>
                            <td>
                                <div class="admin-table-name">
                                    <img
                                        class="admin-table-image"
                                        src="${item.course_image}"
                                    />
                                    <p class="admin-table-name">${item.course_name}</p>
                                </div>
                            </td>
                            <td>
                                <div class="admin-table-type">${author}</div>
                            </td>
                            <td style="text-align:center" >
                                <div class="admin-table-type">${dateOnly[0]}</div>
                            </td>
                            <td style="text-align:center">
                                <div class="admin-table-type">${item.course_total_chapters}</div>
                            </td>
                        </tr>`;
                        SetData.append(Data);
                    });
                }
            },
        });
    });
});
//-------------------------------------------------------------------------
//search user
$(document).ready(function () {
    $("#SearchUser").keyup(function () {
        let Value = $("#SearchUser").val();
        let SetData = $("#DataUsers");
        SetData.html("");
        $.ajax({
            type: "post",
            url: "GetUsersAdmin?Value=" + Value,
            contentType: "html",
            success: function (result) {
                if (result.length == 0) {
                    SetData.append("<tr><td>Not Found</td></tr>");
                } else {
                    console.log(result);
                    result.users.forEach((item) => {
                        if (item.roleID == "3" || item.roleID == "4") {
                            let feeType = ``;
                            result.subscriptions.forEach((sub) => {
                                if (
                                    sub.subscription_id == item.subscriptionId
                                ) {
                                    result.fees.forEach((fee) => {
                                        if (fee.fee_id == sub.fee_id) {
                                            if (fee.fee_id == 1) {
                                                feeType = ` <div class="admin-table-action admin-table-action--purple">
                                                                ${fee.fee_type}
                                                            </div>`;
                                            }
                                            if (fee.fee_id == 2) {
                                                feeType = ` <div class="admin-table-action admin-table-action--orange">
                                                    ${fee.fee_type}
                                                </div>`;
                                            }
                                            if (fee.fee_id == 3) {
                                                feeType = ` <div class="admin-table-action admin-table-action--green">
                                                    ${fee.fee_type}
                                                </div>`;
                                            }
                                        }
                                    });
                                }
                            });
                            let Data = `
                            <tr>
                                <td>
                                    <div class="admin-table-number">
                                        ${item.userID}
                                    </div>
                                </td>
                                <td>
                                    <div class="admin-table-name">
                                        <img
                                            class="admin-table-image"
                                            src="${item.urlAvatar}"
                                            alt=""
                                        />
                                        <p class="admin-table-name">
                                            ${item.fullName}
                                        </p>
                                    </div>
                                </td>
                                <td>
                                    <div class="admin-table-type">
                                        ${item.userEmail}
                                    </div>
                                </td>
                                <td>
                                    ${feeType}
                                </td>
                                <td>
                                ${
                                    item.roleID == "3"
                                        ? `
                                        <div class="admin-table-action admin-table-action--green">
                                            Active
                                        </div>`
                                        : `
                                        <div class="admin-table-action admin-table-action--none">
                                            None
                                        </div>`
                                }
                                </td>
                                <td class="center">
                                    <a
                                        class="admin-table-action admin-table-action--red"
                                        asp-controller="User" asp-action="UpdateRole" asp-route-id="${
                                            item.UserID
                                        }"
                                        ><i class="fas fa-trash"></i
                                    ></a>
                                </td>
                            </tr>
                            `;
                            SetData.append(Data);
                        }
                    });
                }
            },
        });
    });
});
//handle detail infomation teacher
$(document).ready(function () {
    const detaiList = document.querySelectorAll("#detail-user");
    detaiList.forEach((item) => {
        item.addEventListener("click", function () {
            let Value = item.getAttribute("value");
            $.ajax({
                type: "post",
                url: "TeacherAdminJson?Value=" + Value,
                contentType: "html",
                success: function (result) {
                    result.userList.forEach((element) => {
                        if (result.teacher.user_id == element.userID) {
                            let template = `
                                <div class="modal">
                                    <div class="modal-main">
                                        <div class="modal-top">
                                        <div class="modal-title">Profile </div>
                                        <i class="far fa-times modal-close"></i>
                                        </div>
                                        <div class="modal-bottom"> <img class="modal-image" src="${element.urlAvatar}" alt="">
                                        <div class="modal-profile">
                                            <div class="modal-profile-double">
                                            <div class="modal-profile-input">
                                                <h3 class="modal-input-name">Last Name</h3>
                                                <div class="modal-input-value">Sang</div>
                                            </div>
                                            <div class="modal-profile-input">
                                                <h3 class="modal-input-name">First Name</h3>
                                                <div class="modal-input-value">Ha</div>
                                            </div>
                                            </div>
                                            <div class="modal-profile-input">
                                            <h3 class="modal-input-name">Email</h3>
                                            <div class="modal-input-value">${element.userEmail}</div>
                                            </div>
                                            <div class="modal-profile-input">
                                            <h3 class="modal-input-name">Contact number</h3>
                                            <div class="modal-input-value">${element.userContact}</div>
                                            </div>
                                            <div class="modal-profile-input">
                                            <h3 class="modal-input-name">Address</h3>
                                            <div class="modal-input-value">${element.userAddress}</div>
                                            </div>
                                            <div class="modal-profile-input">
                                            <h3 class="modal-input-name">Certificate url</h3><a class="modal-input-value modal-textdecor" href="null">${result.teacher.certificate}</a>
                                            </div>
                                        </div>
                                        </div>
                                        <div class="modal-submit">
                                        <button class="button-base" id="btn-detail"   onclick="handleApprove(this)" value="Approve" data-id="${result.teacher.user_id}">Approve</button>
                                        <button class="button-base button-base--red" id="btn-detail" onclick="handleApprove(this)"value="Reject" data-id="${result.teacher.user_id}">Reject</button>
                                        </div>
                                    </div>
                                </div>`;
                            document.body.insertAdjacentHTML(
                                "beforeend",
                                template
                            );
                            document.body.addEventListener(
                                "click",
                                handleClose
                            );
                            function handleClose(e) {
                                if (e.target.matches(".modal-close")) {
                                    const modal =
                                        e.target.parentNode.parentNode
                                            .parentNode;
                                    modal.parentNode.removeChild(modal);
                                } else if (e.target.matches(".modal")) {
                                    e.target.parentNode.removeChild(e.target);
                                }
                            }
                        }
                    });
                },
            });
        });
    });
});
//ajax commom to use
function handleAjax(method, url, handleResult) {
    $.ajax({
        type: method,
        url: url,
        contentType: "html",
        success: handleResult,
    });
}
//handle reject or accept
function handleApprove(e) {
    let value = e.getAttribute("value");
    let userID = e.getAttribute("data-id");
    handleAjax(
        "post",
        "ApproveTeacher?Value=" + value + "&ID=" + userID,
        handleResultAjax
    );
}
function handleResultAjax(result) {
    if (result.isRedirect) {
        window.location.href = result.redirectUrl;
    }
}
// handle search in teacher
function handleSearchTeacher() {
    let searchTeacher = document.querySelector("#valueTeacher");
    if (searchTeacher) {
        searchTeacher.addEventListener("keyup", function (e) {
            let valueTeacher = e.target.value;
            handleAjax(
                "post",
                "GetTeacherAdmin?Value=" + valueTeacher,
                handleSearchTeacherJson
            );
        });
    }
}
function handleSearchTeacherJson(result) {
    const valueTeacher = document.querySelector(".valueTeacher");
    if (valueTeacher) {
        valueTeacher.innerHTML = "";
        result.teachers.forEach((item) => {
            result.userList.forEach((element) => {
                result.idTeacher.forEach((id) => {
                    if (item.user_id == element.userID && item.user_id == id) {
                        let templte = `
                <tr>
                    <td>
                        <div class="admin-table-number">${element.userID}</div>
                    </td>
                    <td>
                        <div class="admin-table-name">
                            <img
                                class="admin-table-image"
                                src="${element.urlAvatar}"
                                alt=""
                            />
                            <p class="admin-table-name">${element.userEmail}</p>
                        </div>
                    </td>
                    <td>
                        <div class="admin-table-type">${element.fullName}</div>
                    </td>
                    <td>
                        <a
                            class="admin-table-action admin-table-action--blue"
                            href="#"
                            ><i class="far fa-comment"></i
                        ></a>
                    </td>
                    <td>
                        <a
                            class="admin-table-action admin-table-action--red"
                            href="#"
                            ><i class="fas fa-trash"></i
                        ></a>
                    </td>
                </tr>
                `;
                        valueTeacher.insertAdjacentHTML("afterbegin", templte);
                    }
                });
            });
        });
    }
}
function handleFeedbackTeacher() {
    const btnFeeback = document.querySelectorAll("#btn-feedbackTeacher");
    btnFeeback.forEach((item) => {
        item.addEventListener("click", function (e) {
            let idUser = e.target.getAttribute("data-id");
            const template = `
                    <div class="modal">
                        <div class="modal-main">
                            <div class="modal-top">
                            <div class="modal-title">Feedback </div>
                                <i class="far fa-times modal-close"></i>
                            </div>
                            <div class="modal-desc">
                                <textarea class="modal-textarea" rows="5" required></textarea>
                            </div>
                            <p class="modal-noti">
                                You can't leave it blank
                            </p>
                            <div class="modal-submit">
                                <button class="button-base" id="btn-detail" onclick="handleClickSendFeedback(this)" data-id-user="${idUser}">Send</button>
                                <button class="button-base button-base--red" id="btn-detail">Cancel</button>
                        </div>
                    </div>
                    `;
            document.body.insertAdjacentHTML("beforeend", template);
            document.body.addEventListener("click", handleClose);
            function handleClose(e) {
                if (e.target.matches(".modal-close")) {
                    const modal = e.target.parentNode.parentNode.parentNode;
                    modal.parentNode.removeChild(modal);
                } else if (e.target.matches(".modal")) {
                    e.target.parentNode.removeChild(e.target);
                }
            }
        });
    });
}
function handleClickSendFeedback(e) {
    const idUser = e.getAttribute("data-id-user");
    const textValue = document.querySelector(".modal-textarea").value;
    const modalNoti = document.querySelector(".modal-noti");
    if (textValue == "") {
        modalNoti.classList.add("active");
    } else {
        modalNoti.classList.remove("active");
        handleAjax(
            "post",
            "FeedbackTeacher?Value=" + textValue + "&id=" + idUser,
            handleResultAjax
        );
    }
}
function handleUpdateCourseByID() {
    const btnUpdateID = document.querySelectorAll("#btnUpdateCourse");
    if (btnUpdateID) {
        btnUpdateID.forEach((item) => {
            item.addEventListener("click", function (e) {
                let value = e.target.getAttribute("data-id-course");
                handleAjax(
                    "post",
                    "/Course/AddCourseByIDJson?id=" + value,
                    handleResultAjax
                );
            });
        });
    }
}
function handleInsertChapter(e) {
    const courseID = e.getAttribute("data-id-course");
    handleAjax(
        "post",
        "/Chapter/AddChapter?idCourse=" + courseID,
        handleChapterToView
    );
}
function handleChapterToView(result) {
    console.log(result);
    const chapterList = document.querySelector(".teacher-chapter-list");
    if (chapterList) {
        chapterList.innerHTML = "";
        result.list.forEach((item) => {
            let templete = `
            <div class="teacher-chapter-item">
                <h3 class="teacher-chapter-title">${item.chapter_name}</h3>
                <div class="teacher-chapter-icon">
                    <a class="admin-table-action admin-table-action--blue quiz-delete" asp-route-chapterID="${item.chapter_id}" asp-route-idCourse="${result.id}" asp-action="ManageChapterDetail" asp-controller="Chapter" >
                        <i class="fas fa-pen"></i>
                    </a>
                    <a class="admin-table-action admin-table-action--red quiz-delete" onclick="handleDeleteChapter(this)" data-id-chapter="${item.chapter_id}" data-id-course="${result.id}">
                        <i class="fas fa-trash"></i>
                    </a>
                </div>
            </div>
        `;
            chapterList.insertAdjacentHTML("afterbegin", templete);
        });
    }
}
function handleDeleteChapter(e) {
    const courseID = e.getAttribute("data-id-course");
    const chapterID = e.getAttribute("data-id-chapter");
    handleAjax(
        "post",
        "/Chapter/ChapterDelete?courseID=" +
            courseID +
            "&chapterID=" +
            chapterID,
        handleChapterToView
    );
}

handleUpdateCourseByID();
handleFeedbackTeacher();
handleSearchTeacher();
function handleDeleteQuestion(e) {
    handleAjax(
        "Post",
        "/Chapter/DeleteQuesiton?questionId=" +
            e.getAttribute("data-id-quesiton") +
            "&chapterId=" +
            e.getAttribute("data-id-chapter"),
        handleResultAjax
    );
}
window.addEventListener("load", function () {
    const fquestions = document.querySelectorAll("#fquestion");
    fquestions.forEach((fq) => {
        fq.addEventListener("submit", function (e) {
            const btnAction = e.target.querySelector(".btn-submit").value;
            e.preventDefault();
            console.log();
            let formData = new FormData();
            formData.append(
                "vocabulary",
                // e.target.forms["formQuestion"]["vocabulary"].value
                e.target.querySelector("input[name=vocabulary]").value
            );
            formData.append(
                "ipa",
                e.target.querySelector("input[name=ipa]").value
            );
            formData.append(
                "meaning",
                e.target.querySelector("input[name=meaning]").value
            );
            formData.append(
                "type",
                e.target.querySelector("input[name=type]").value
            );
            formData.append(
                "ex",
                e.target.querySelector("input[name=ex]").value
            );

            let fileImage = document.querySelector("input[name=file]").files[0];
            formData.append("file", fileImage);
            formData.append(
                "chapterID",
                e.target.querySelector("input[name=chapterID]").value
            );
            if (btnAction == "Update") {
                formData.append(
                    "wordID",
                    e.target.querySelector("input[name=wordID]").value
                );
                $.ajax({
                    type: "POST",
                    url: "/Chapter/UpdateQuestionChapter",
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: handleRenderQuestions,
                });
            } else if (btnAction == "Add") {
                $.ajax({
                    type: "POST",
                    url: "/Chapter/AddQuestionChapter",
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: handleRenderQuestions,
                });
            }
        });
    });
});

function handleRenderQuestions(result) {
    const chapterList = document.querySelector(".chapter-list");
    chapterList.innerHTML = "";
    let count = 0;
    result.wordList.forEach((item) => {
        result.wordTypeList.forEach((elemnt) => {
            if (elemnt.wordtype_id == item.wordType_id) {
                let listOptions = [];
                result.wordTypeList.forEach((option) => {
                    listOptions.push(
                        `<div class="options-item">${option.wordtype_name}</div>`
                    );
                });
                console.log(result);
                count++;
                let templete = `
                <form method="post" id="fquestion" name="formQuestion" enctype="multipart/form-data" class="chapter-item">
                <div class="chapter-item-group">
                    <h3 class="chapter-item-name">
                        Quesion ${count}
                    </h3>
                    <i class="fas fa-angle-up"></i>
                </div>
                <div class="chapter-update-container">
                    <div class="chapter-update-main">
                        <div class="chapter-update-top">
                            <div class="input-base">
                                <div class="input-base-title">
                                    Vocabulary
                                </div>
                                <input
                                    type="text" value="${item.word_face}"
                                    placeholder="Enter your vocaluary" name="vocabulary"
                                />
                            </div>
                            <div class="input-base">
                                <div class="input-base-title">
                                    IPA
                                </div>
                                <input
                                    type="text" value="${item.word_IPA}"
                                    placeholder="Enter your vocaluary"
                                    name="ipa"
                                />
                            </div>
                        </div>
                        <div class="input-base">
                            <div class="input-base-title">
                                Meaning
                            </div>
                            <input
                                type="text"
                                placeholder="Enter your meaning word"
                                value=${item.word_nghia}"
                                name="meaning"
                            />
                        </div>
                        <div class="input-base">
                            <div class="input-base-title">
                                Type of word
                            </div>
                            <div class="selection">
                                        <input
                                    type="text"
                                    class="selected"
                                    placeholder="Value"
                                    data-id-type="${elemnt.wordType_id}"
                                    value="${elemnt.wordtype_name}"
                                    name="type"
                                    readonly
                                />

                                <div class="options-container">
                                    ${listOptions}
                                </div>
                            </div>
                        </div>
                        <div class="input-base">
                            <div class="input-base-title">
                                For example
                            </div>
                            <input
                                type="text"
                                placeholder="Enter your example for word"
                                value="${item.word_phrase}"
                                name="ex"
                            />
                        </div>
                        <div class="chapter-update-top">
                            <div class="input-base">
                                <div class="input-base-title">
                                    Images
                                </div>
                                <div class="input-file">
                                    <input
                                        id="file"
                                        type="file"
                                        name="file"
                                        value="${item.word_image}"
                                        class="file-images"
                                    />
                                    <img class="image-word" src="${item.word_image}" alt="">
                                    <label
                                        class="input-label"
                                        for="file"
                                        ><i
                                            class="fas fa-cloud-upload-alt icon-upload"
                                        ></i
                                    ></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="submit-container">
                        <button
                            class="btn-submit"
                            type="submit"
                        >
                            Update
                        </button>
                        <a class="btn-submit btn-submit--normal" data-id-quesiton="${item.word_id}" data-id-chapter="${result.chapterID}" onclick="handleDeleteQuestion(this)">
                            Delete
                        </a>
                    </div>
                </div>
            </form>
                `;
                chapterList.insertAdjacentHTML("beforeend", templete);
            }
        });
    });
}
