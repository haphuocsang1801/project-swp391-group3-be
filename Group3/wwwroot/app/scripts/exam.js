const questionnums = document.querySelectorAll(".button-question[data-number]");
const questioncontent = document.querySelectorAll(
    ".table-question-content[data-content]"
);
window.onload = () => {
    questionnums.forEach((questionbtn) => {
        questionbtn.addEventListener("click", () => {
            const currentBtn = document.querySelector(
                ".button-question.active"
            );
            const questionid = questionbtn.dataset.number;
            currentBtn.classList.remove("active");
            questionbtn.classList.add("active");

            switchQuestion(questionid);
        });
    });

    document
        .querySelector(".button-question:first-child")
        .classList.add("active");
    document
        .querySelector(".table-question-content:first-child")
        .classList.add("active");
};

function switchQuestion(id) {
    const currentquestion = document.querySelector(
        ".table-question-content.active"
    );
    currentquestion.classList.remove("active");

    const nextquestion = document.querySelector(
        `.table-question-content[data-content="${id}"]`
    );
    console.log(nextquestion);
    nextquestion.classList.add("active");
}
let rightAnswers = 0;
function finishTheTest() {
    const userAnswers = document.querySelectorAll(".option-content .answer");
    userAnswers.forEach((element) => {
        if (element.checked == true) {
            if (element.getAttribute("value")) {
                rightAnswers += 1;
            }
        }
    });
    return rightAnswers;
}
const btnFinish = document.querySelector(".button-finish");
if (btnFinish) {
    btnFinish.addEventListener("click", function (e) {
        let id = e.target.getAttribute("data-id");
        let rightAnswers = finishTheTest();
        console.log(rightAnswers);
        handleAnswerAjax(
            "post",
            "/Exam/GetTotalRightAnswer?value=" + rightAnswers + "&id=" + id,
            handleResultAnswerAjax
        );
    });
}

function handleAnswerAjax(method, url, handleResult) {
    $.ajax({
        type: method,
        url: url,
        contentType: "html",
        success: handleResultAnswerAjax,
    });
}
function handleResultAnswerAjax(result) {
    if (result.isRedirect) {
        window.location.href = result.redirectUrl;
    }
}
const ExamRemove = document.querySelectorAll("#ExamRemove");
if (ExamRemove) {
    ExamRemove.forEach((item) => {
        item.addEventListener("click", function (e) {
            let value = e.target.getAttribute("data-id-exam");
            handleAnswerAjax(
                "post",
                "/Exam/DeleteExam?id=" + value,
                handleResultAnswerAjax
            );
        });
    });
}
