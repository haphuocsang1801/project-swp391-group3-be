var synth = window.speechSynthesis;
var utterance = new SpeechSynthesisUtterance("Hello World");
if ("speechSynthesis" in window) {
    var synthesis = window.speechSynthesis;
    // Get the first `en` language voice in the list
    var voice = synthesis.getVoices().filter(function (voice) {
        return voice.lang === "en";
    })[0];

    // Create an utterance object
    var utterance = new SpeechSynthesisUtterance("Hello World");

    // Set utterance properties
    utterance.voice = voice;
    utterance.pitch = 2;
    utterance.rate = 1.25;
    utterance.volume = 0.6;
} else {
    console.log("not SUpported");
}

const wordFlipCards = document.querySelectorAll(
    ".div-learn-content[data-word-number] .div-learn-card"
);
const continueBtn = document.querySelector(".continue-button");
const textBoxes = document.querySelectorAll(".textbox");
const fillWordTextBoxes = document.querySelectorAll(".fill-in-the-word");
const popup = document.querySelector(".div-answer");
const listenBtns = document.querySelectorAll(".div-audio-icon");
const contentDivs = document.querySelectorAll(".div-learn-content");
const progressBar = document.querySelector(".progress-bar");
const openModal = document.querySelector(".feedbackBtn");
const modal = document.querySelector(".feedbackmodal");
const closeModal = document.querySelector(".closeModal");
var answer = false;
var change = Math.floor(600 / wordFlipCards.length);
console.log(change);
window.onload = () => {
    document
        .querySelector(".div-learn-content:first-child")
        .classList.add("active");
};

openModal.addEventListener("click", () => {
    modal.show();
});

closeModal.addEventListener("click", () => {
    modal.close();
});

wordFlipCards.forEach((wordfc) => {
    wordfc.addEventListener("click", () => {
        if (!ContinueBtnState()) {
            let answer = wordfc.parentElement.getAttribute("data-answer");
            utterance.text = answer;
            synthesis.speak(utterance);
        }
        turnOnButton();
    });
});
var incorrectQuestion;
continueBtn.addEventListener("click", () => {
    var currentContent = document.querySelector(".div-learn-content.active");
    let popup = document.querySelector(
        `.div-answer[data-answer="${currentContent.getAttribute(
            "data-answer"
        )}"]`
    );
    if (
        ContinueBtnState() &&
        document
            .querySelector(".div-learn-content.active")
            .hasAttribute("data-word-number")
    ) {
        turnOffButton();
        var nextContent = currentContent.nextElementSibling;
        nextContent.classList.add("active");
        currentContent.classList.remove("active");
        //Progress bar
        if (currentContent.getAttribute("data-word-number") == 0) {
            progressBar.setAttribute("style", `width:${change}px`);
        } else {
            progressBar.setAttribute("style", `width:${change}px`);
        }
    } else if (ContinueBtnState() && PopupState(popup)) {
        //Popup present continue
        turnOffButton();
        let nextContent;
        if (currentContent.querySelector(".textbox") != null) {
            currentContent.querySelector(".textbox").value = "";
        }
        if (currentContent.querySelector(".fill-in-the-word") != null) {
            currentContent.querySelector(".fill-in-the-word").value = "";
        }
        if (
            currentContent.nextElementSibling.classList.contains("div-answer")
        ) {
            nextContent = currentContent.nextElementSibling.nextElementSibling;
        } else {
            nextContent = currentContent.nextElementSibling;
        }
        if (
            document
                .querySelectorAll(".div-learn-content")
                [contentDivs.length - 1].classList.contains("active")
        ) {
            //Done learning
            window.location.href = "/LearningPage/AddWordAndChapter";
        }
        nextContent.classList.add("active");
        currentContent.classList.remove("active");
        if (popup.classList.contains("correct")) {
            popup.classList.remove("correct");
        } else {
            popup.classList.remove("incorrect");
            insertAfter(
                document.querySelectorAll(".div-learn-content")[
                    contentDivs.length - 1
                ],
                incorrectQuestion
            );
        }
    } else if (ContinueBtnState() && !PopupState(popup)) {
        //Bring up popup if not present
        if (answer) {
            popup.classList.add("correct");
        } else popup.classList.add("incorrect");
        incorrectQuestion = document.querySelector(".div-learn-content.active");
        answer = false;
    }
});

textBoxes.forEach((textbox) => {
    textbox.addEventListener("keyup", (event) => {
        if (textbox.value != "") {
            turnOnButton();
        } else turnOffButton();
        answer = checkTextBoxAnswer(textbox);
        if (event.keyCode === 13) {
            continueBtn.click();
        }
    });
});

fillWordTextBoxes.forEach((fwtextbox) => {
    fwtextbox.addEventListener("keyup", (event) => {
        if (fwtextbox.value != "") {
            turnOnButton();
        } else {
            turnOffButton;
        }
        answer = checkTextBoxAnswer(fwtextbox);
        if (event.keyCode === 13) {
            continueBtn.click();
        }
    });
});

listenBtns.forEach((btn) => {
    btn.addEventListener("click", () => {
        const listen_answer = document
            .querySelector(".div-learn-content.active")
            .getAttribute("data-answer");
        utterance.text = listen_answer;
        synthesis.speak(utterance);
    });
});

function insertAfter(referenceNode, newNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}
function checkTextBoxAnswer(textbox) {
    return (
        textbox.value.toLowerCase() ==
        textbox.parentElement.getAttribute("data-answer").toLowerCase()
    );
}

function PopupState(popup) {
    return (
        popup.classList.contains("correct") ||
        popup.classList.contains("incorrect")
    );
}

function ContinueBtnState() {
    return continueBtn.classList.contains("active");
}

function turnOffButton() {
    continueBtn.classList.remove("active");
}

function turnOnButton() {
    continueBtn.classList.add("active");
}

for (const frontOrBack of document.querySelectorAll(".div-learn-card *")) {
    frontOrBack.addEventListener("click", () => {
        frontOrBack.classList.toggle("flipped");
    });
}
