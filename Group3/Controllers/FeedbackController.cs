using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Group3.Models.DAO;
using Group3.Models;
//using Group3.Models;

namespace Group3.Controllers
{
    public class FeedbackController : Controller
    {
        UserDAO dao= new UserDAO();
        public FeedbackController()
        {
        }

        public IActionResult Index()
        {
            List<User> UserList=new List<User>();
            UserList=dao.getListUser();
            ViewData["Feedbacks"]=FeedbackDAO.GetFeedback("");
            ViewData["Users"]=UserList;
            return View();
        }
        public IActionResult FeedBackTeacher(){
            List<User> UserList=new List<User>();
            UserList=dao.getListUser();
            ViewData["Feedbacks"]=FeedbackDAO.GetFeedback("");
            ViewData["Users"]=UserList;
            return View();
        }
        public IActionResult UpdateStatus(bool status,int id,int role){
            FeedbackDAO.UpdateFeedback(id,status);
            if(role==2){
                return RedirectToAction("FeedBackTeacher");
            }
            else{
                return RedirectToAction("Index");
            }
        }
        public JsonResult SearchFeedback(string value){
            List<User> UserList=new List<User>();
            UserList=dao.getListUser();
            List<Feedback> feedbacksList=FeedbackDAO.GetFeedback(value);
            var result = new{
                userList=UserList,feedbacksList=feedbacksList
            };
            return Json(result);
        }
        public JsonResult SearchFeedbackByIDTeacher(string value){
            List<User> UserList=new List<User>();
            var IDteacher=HttpContext.Session.GetString("userID");
            UserList=dao.getListUser();
            List<Feedback> feedbacksList=FeedbackDAO.GetFeedbackByTeacherID(value,int.Parse(IDteacher));
            var result = new{
                userList=UserList,feedbacksList=feedbacksList
            };
            return Json(result);
        }
    }
}