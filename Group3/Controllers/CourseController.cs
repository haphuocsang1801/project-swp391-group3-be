using Group3.Helpper;
using Group3.Models;
using Group3.Models.DAO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Linq;

namespace Group3.Controllers
{
    public class CourseController:Controller
    {
        CourseDAO dao = new CourseDAO();
        ChapterDAO chapterDAO = new ChapterDAO();
        public IActionResult CourseAdmin(){
            ViewData["Courses"]=dao.GetCourses();
            ViewData["Users"]= new UserDAO().getListUser();
            ViewData["Teachers"]= TeacherDAO.getTeacherList();
            return View();
        }
        public JsonResult GetCourseAdmin(string SearchValue){
            List<Course> CourseList=new List<Course>();
            CourseList=dao.GetCourseByIDandName(SearchValue);
            List<User> UserList= new UserDAO().getListUser();
            ViewModelCourseUser obj= new ViewModelCourseUser();
            obj.Users=UserList;
            obj.Courses=CourseList;
            return Json(obj);
        }
        public IActionResult AddCourse(){
            var IdTeacher = HttpContext.Session.GetString("UserID");
            int IDcourseInsert;
            bool check=dao.InsertCourse("",0,int.Parse(IdTeacher),"",0);
            IDcourseInsert=dao.getIDCourseLaster();
            Course obj= dao.GetCourseByID(IDcourseInsert);
            ViewData["Chapters"] = chapterDAO.GetChaptersByCourseId(IDcourseInsert);
            ViewData["Course"]=obj;
            return View();
        }
        public JsonResult AddCourseByIDJson(int id){
            HttpContext.Session.SetString("IDcourse", id.ToString());
            return Json(new{
                redirectUrl = Url.Action("AddCourseByID", "Course"),
                isRedirect = true
            });
        }
        public IActionResult AddCourseByID(){
            int idCourse=int.Parse(HttpContext.Session.GetString("IDcourse"));
            var IdTeacher = HttpContext.Session.GetString("UserID");
            Course obj= dao.GetCourseByID(idCourse);
            ViewData["Chapters"] = chapterDAO.GetChaptersByCourseId(idCourse);
            ViewData["Course"]=obj;
            if(TempData["Duplicate"]!=null){
                TempData["Duplicate"]="Name already exits";
            }
            if(TempData["EmptyImage"]!=null){
                TempData["EmptyImage"]="Please fill images";
            }
            return View("~/Views/Course/AddCourse.cshtml");
        }
        public IActionResult UpdateCourse(string nameCourse,IFormFile file,string courseID){
            Course course= dao.CheckDuplicateCourse(nameCourse);
            //chap nhan ten trung voi course dang cap nhat
            if(file==null){
                TempData["EmptyImage"]="Please fill images";
                TempData["IDcourse"]=int.Parse(courseID);
                return RedirectToAction("AddCourseByID","Course");
            }
            if(nameCourse==dao.GetCourseByID(int.Parse(courseID)).course_name || course==null){
                var IdTeacher = HttpContext.Session.GetString("UserID");
                Utilities.UpLoadImage(file);
                string fileName="/images/"+file.FileName;
                bool check=dao.UploadCourse(nameCourse,fileName,int.Parse(courseID));
            }
            if(course!=null){
                TempData["Duplicate"]="Name already exits";
                TempData["IDcourse"]=int.Parse(courseID);
                return RedirectToAction("AddCourseByID","Course");
            }
            return RedirectToAction("OverView", "Teacher");
        }
        [HttpGet]
        [Route("/Course/DeleteCourse/{CourseID}")]
        public IActionResult DeleteCourse(string CourseID){
            dao.DeleteCourse(int.Parse(CourseID));
            chapterDAO.DeleteAllChapterByCourseID(int.Parse(CourseID));
            return RedirectToAction("OverView", "Teacher");
        }
    }
    public class ViewModelCourseUser
        {
            public IEnumerable<User> Users { get; set; }
            public IEnumerable<Course> Courses { get; set; }
        }
}