using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Group3.Models.DAO;
using Microsoft.AspNetCore.Mvc;
//using Group3.Models;

namespace Group3.Controllers
{
    public class reportController : Controller
    {
        PaymentDAO paymentDAO = new PaymentDAO();
        UserDAO userDAO= new UserDAO();
        public reportController()
        {
        }

        public IActionResult Index()
        {
            ViewData["TotalMoney"]=Math.Round(paymentDAO.getTotalEearning(), 2);
            ViewData["userPayments"]=paymentDAO.getListUserPayment();
            ViewData["users"]=userDAO.getListUser();
            ViewData["fees"]=paymentDAO.getListFee();
            ViewData["methodPayments"]=paymentDAO.getListPaymentMethod();
            return View();
        }
        public JsonResult handleChartByWeekly(){
            DayOfWeek weekStart = DayOfWeek.Monday; // or Sunday, or whenever
            DateTime startingDate = DateTime.Today;

            while(startingDate.DayOfWeek != weekStart)
                startingDate = startingDate.AddDays(-1);

            DateTime previousWeekStart = startingDate.AddDays(-7);
            List<double> chartList= new List<double>();
            for (var i = 0; i <=6 ; i++)
            {
                DateTime Addday =previousWeekStart.AddDays(i);
                chartList.Add(paymentDAO.getTotalEearningDay(Addday));
            }
            return Json(chartList);
        }
        public JsonResult hanleChartByMonthLy(){
            var today = DateTime.Today;
            var month = new DateTime(today.Year, today.Month, 1);
            var first = month.AddMonths(-1);
            var last = month.AddDays(-1);
            List<double> chartList= new List<double>();
            int countDays=last.Day;
            for (var i = 0; i < countDays; i++)
            {
                chartList.Add(paymentDAO.getTotalEearningDay(first.AddDays(i)));
            }
            return Json(chartList);
        }
        public JsonResult hanleChartByQuarterLy(){
            var today = DateTime.Today;
            var month = new DateTime(today.Year, today.Month, 1);
            var first = month.AddMonths(-1);
            var last = month.AddDays(-1);
            List<double> chartList= new List<double>();
            int countDays=last.Day;
            for (var i = 0; i < countDays; i++)
            {
                chartList.Add(paymentDAO.getTotalEearningDay(first.AddDays(i)));
            }
            return Json(chartList);
        }
    }
}