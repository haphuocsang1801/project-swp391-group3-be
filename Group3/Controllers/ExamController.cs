using System.Dynamic;
using Group3.Models;
using Group3.Models.DAO;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
//using Group3.Models;

namespace Group3.Controllers
{
    public class ExamController : Controller
    {
        ExamDAO exDao = new ExamDAO();
        WordDAO wDao = new WordDAO();
        QuestionDAO qDao = new QuestionDAO();
        UserDAO userDao= new UserDAO();
        [HttpGet]
        [Route("/Exam/Index/{exam_id}")]
        public  async Task<IActionResult> Index(int exam_id)
        {
            ViewData["ExamById"] = exDao.GetExamById(exam_id);
            ViewData["Examination"] = exDao.getListExam();
            return View();
        }
        [HttpGet]
        [Route("/Exam/Process/{exam_id}")]
        public async Task<IActionResult> Process(int exam_id)
        {
            List<Questions> questionlist = new List<Questions>();
            List<Word> wordlist = new List<Word>();
            questionlist = exDao.GetQuestionsByExamID(exam_id);
            foreach (var question in questionlist)
            {
                wordlist.Add(wDao.GetWord(question.word_id));
            }
            ViewData["ExamById"] = exDao.GetExamById(exam_id);
            ViewData["Examination"] = exDao.getListExam();
            ViewData["WordsForQuestions"] = wordlist;
            ViewData["GetQAforExam"] = questionlist;
            ViewData["GetWords"] = wDao.GetWord(exam_id);

            return View();
        }
        public IActionResult UserInfo()
        {
            return View();
        }
        public JsonResult GetTotalRightAnswer(int value,int id){
            TempData["rightAnswer"] =value;
            return Json(new{
                redirectUrl = Url.Action("Result", "Exam",new {
                    id=id
                }),
                isRedirect = true
            });
        }
        public  async Task<IActionResult> Result(int id)
        {
            TempData["right"] = TempData["rightAnswer"];
            ViewData["ExamById"] = exDao.GetExamById(id);
            return View();
        }
        public RedirectResult goToExam(){
            string goToExam = "/Exam/Index";
            return Redirect(goToExam);
        }

        public IActionResult ListExam()
        {
            ViewData["Examination"] = exDao.getListExam();
            return View();
        }
        public RedirectResult StartExam()
        {
            string startExam = "/Exam/Process";
            return Redirect(startExam);
        }
        public RedirectResult ReviewExam()
        {
            string reviewExam = "/Exam/ListExam";
            return Redirect(reviewExam);
        }
        public RedirectResult backToHome()
        {
            string backToHome = "/Admin/Index";
            return Redirect(backToHome);
        }
        public RedirectResult backToCourse()
        {
            string backToCourse = "/Admin/Index";
            return Redirect(backToCourse);
        }
        public RedirectResult backListExam()
        {
            string backListExam = "/Exam/ListExam";
            return Redirect(backListExam);
        }
        public RedirectResult goToListExam(){
            string goToListExam = "/Exam/ListExam";
            return Redirect(goToListExam);
        }
        public RedirectResult reviewPage(){
            string reviewPage = "/Reviewing/Index";
            return Redirect(reviewPage);
        }
        public RedirectResult continueLearning()
        {
            string continueLearning = "/LearningPage/Index";
            return Redirect(continueLearning);
        }
        public RedirectResult Exit()
        {
            string exit = "/Admin/Index";
            return Redirect(exit);
        }
        public RedirectResult backToUserProfile()
        {
            string backToUserProfile = "/Exam/UserInfo";
            return Redirect(backToUserProfile);
        }
        public async Task<IActionResult> ExamAdmin(){
            ViewData["Examination"] = exDao.getListExam();
            ViewData["users"]=userDao.getListUser();
            return View();
        }
        public JsonResult DeleteExam(int id){
            exDao.DeleteExam(id);
            exDao.DelteExamTeacher(id);
            exDao.DeleteExamQuestions(id);
            return Json(new{
                redirectUrl = Url.Action("ExamAdmin", "Exam"),
                isRedirect = true
            });
        }
    }
    public class ViewModelExam
    {
        public IEnumerable<Exam> Examination { get; set; }
        public IEnumerable<Questions> Questions { get; set; }
        public IEnumerable<Word> Words { get; set; }
    }
}