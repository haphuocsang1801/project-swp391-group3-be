﻿using System.Data.Common;
using System.Dynamic;
using Group3.Models;
using Group3.Models.DAO;
using Microsoft.AspNetCore.Mvc;

namespace Group3.Controllers
{
    public class AdminController : Controller
    {
            PaymentDAO paymentDAO = new PaymentDAO();
            CourseDAO courseDAO= new CourseDAO();
            UserDAO userDAO = new UserDAO();

        public IActionResult Index()
        {
            var check = HttpContext.Session.GetString("UserID");
            if (check == null)
            {
                check = "0";
            }
            int userID = int.Parse(check);
            UserDAO dao = new UserDAO();
            string? roleID = dao.getRoleUser(userID);
            if (roleID != "1")
            {
                return RedirectToAction("Index", "Home");
            }
            TempData["totalUser"]=getTotalStudent().ToString();
            TempData["totalCourse"]=getTotalCourse().ToString();
            TempData["totalPayment"]=getTotalPayment().ToString();
            ViewData["userPayments"]=paymentDAO.getListUserPayment();
            ViewData["users"]=userDAO.getListUser();
            ViewData["fees"]=paymentDAO.getListFee();
            ViewData["methodPayments"]=paymentDAO.getListPaymentMethod();
            return View();
        }
        public int getTotalStudent(){
            return userDAO.getTotalUser();
        }
        public int getTotalCourse(){
            return courseDAO.totalCourse();
        }
        public int getTotalPayment(){
            return paymentDAO.totalPayment();
        }
        public List<Payment> getListPayment(){
            return paymentDAO.getListUserPayment();
        }
        public List<User> getUsers(){
            UserDAO obj = new UserDAO();
            return  obj.getListUser();
        }

    }
    public class ViewModel
        {
            public IEnumerable<User> Users { get; set; }
            public IEnumerable<Payment> Payments { get; set; }
        }
}
