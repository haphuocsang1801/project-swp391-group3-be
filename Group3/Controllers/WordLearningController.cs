﻿using Group3.Models;
using Group3.Models.DAO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Group3.Controllers
{
    public class WordLearningController : Controller
    {
        CourseDAO courseDAO = new CourseDAO();
        public IActionResult Learn()
        {
            ViewData["WordsForChapter"] = JsonConvert.DeserializeObject<List<Word>>(HttpContext.Session.GetString("UserLearningSession"));
            ViewData["teacher"] = courseDAO.GetTeacherIDByChapterID(int.Parse(HttpContext.Session.GetString("chapter_id")));
            ViewData["userID"] = HttpContext.Session.GetString("UserID");
            return View();
        }
        
        public IActionResult SendFeedBack(string content,int user_id, int recipient_id){
            if (!string.IsNullOrEmpty(content))
            {
                FeedbackDAO.InsertFeedBack(content,user_id, recipient_id);
                return Json(true);
            }

            return Json(false);
        }
    }
}
