﻿using Group3.Models;
using Group3.Models.DAO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Group3.Controllers
{
    public class LearningPageController : Controller
    {
        CourseDAO courseDAO = new CourseDAO();
        ChapterDAO chapterDAO = new ChapterDAO();
        WordDAO wordDAO = new WordDAO();
        NotebookDAO notebookDAO = new NotebookDAO();
        Notebook notebook = new Notebook();
        UserDAO userDAO = new UserDAO();
        public IActionResult Index()
        {   
            Dictionary<int , int> wordAmountWithLevel = new Dictionary<int, int>();
            Dictionary<int , List<Word>> wordsPerLevel = new Dictionary<int, List<Word>>();
            notebook = notebookDAO.GetNotebookFromUserID(int.Parse(HttpContext.Session.GetString("UserID")));
            List<Chapter> completedChapterList = chapterDAO.GetChaptersCompleted(notebook);
            List<string> leaderboardList = userDAO.GetTop10UserByWordAmount();
            //Getting amount of words & the words per level in notebook
            for (int level = 1; level <= 5; level++)
            {
                wordAmountWithLevel.Add(level, notebookDAO.GetNumberOfWordFromNotebookWithLevel(notebook, level));
                wordsPerLevel.Add(level, notebookDAO.GetNotebookWordsWithLevel(notebook, level));
            }
            ViewData["LeaderBoardList"] = leaderboardList;
            ViewData["CompletedChapters"] = completedChapterList;
            ViewData["NotebookwordsPerLevel"] = wordsPerLevel;
            ViewData["NotebookWordsAmountPerLevel"] = wordAmountWithLevel;
            ViewData["Courses"] = courseDAO.GetCourses();
            ViewData["ChapterForCourse"] = chapterDAO.GetChaptersByCourseId(1);
            return View();
        }

        public IActionResult Select(int course_id)
        {
            Dictionary<int , int> wordAmountWithLevel = new Dictionary<int, int>();
            Dictionary<int , List<Word>> wordsPerLevel = new Dictionary<int, List<Word>>();
            notebook = notebookDAO.GetNotebookFromUserID(int.Parse(HttpContext.Session.GetString("UserID")));
            List<Chapter> completedChapterList = chapterDAO.GetChaptersCompleted(notebook);
            List<string> leaderboardList = userDAO.GetTop10UserByWordAmount();
            //Getting amount of words per level in notebook
            for (int level = 1; level <= 5; level++)
            {
                wordAmountWithLevel.Add(level, notebookDAO.GetNumberOfWordFromNotebookWithLevel(notebook, level));
                wordsPerLevel.Add(level, notebookDAO.GetNotebookWordsWithLevel(notebook, level));
            }
            ViewData["LeaderBoardList"] = leaderboardList;
            ViewData["CompletedChapters"] = completedChapterList;
            ViewData["NotebookwordsPerLevel"] = wordsPerLevel;
            ViewData["NotebookWordsAmountPerLevel"] = wordAmountWithLevel;
            ViewData["Courses"] = courseDAO.GetCourses();
            ViewData["ChapterForCourse"] = chapterDAO.GetChaptersByCourseId(course_id);
            return View("Index");
        }

        public IActionResult Learning(int chapter_id)
        {
            List<Word> words = new List<Word>();
            words = wordDAO.GetWordsByChapterID(chapter_id);
            HttpContext.Session.SetString("UserLearningSession", JsonConvert.SerializeObject(words));
            HttpContext.Session.SetString("chapter_id", chapter_id.ToString());
            return RedirectToAction("Learn", "WordLearning");
        }

        public IActionResult AddWordAndChapter()
        {
          
            notebook = notebookDAO.GetNotebookFromUserID(int.Parse(HttpContext.Session.GetString("UserID")));
            Chapter chapter = chapterDAO.GetChapterByID(int.Parse(HttpContext.Session.GetString("chapter_id")));
            List<Word> newWordsLearned = JsonConvert.DeserializeObject<List<Word>>(HttpContext.Session.GetString("UserLearningSession"));
            notebookDAO.AddWordsIntoNotebook(newWordsLearned, notebook);
            //Getting amount of words per level in notebook
            chapterDAO.addCompletedChapter(notebook, chapter);
            return RedirectToAction("Index");
        }

        public JsonResult GetSearchData(string SearchLevel, string SearchValue)
        {
            List<Word> wordlist= new List<Word>();
            try
            {
                notebook = notebookDAO.GetNotebookFromUserID(int.Parse(HttpContext.Session.GetString("UserID")));
                wordlist = wordDAO.GetWordsByNameInLevelInNotebook(notebook, SearchValue, SearchLevel);
            }
            catch (Exception ex)
            {

                throw;
            }
            return Json(wordlist);
        }
    }
}
