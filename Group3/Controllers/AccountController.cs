﻿using System.Net;
using System.Net.Mail;
using Group3.Helpper;
using Group3.Models;
using Group3.Models.DAO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Group3.Controllers
{
    public class AccountController : Controller
    {
        UserDAO dao = new UserDAO();
        public IActionResult Login()
        {
            var check = HttpContext.Session.GetString("UserID");
            if (check == null)
            {
                return View();
            }
            int? userID = int.Parse(check);
            string? roleID = dao.getRoleUser(userID);
            if (roleID == "1")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (roleID.Equals("2") && TeacherDAO.GetTeacherActive(userID))//teacher
            {
                return RedirectToAction("OverView", "Teacher");
            }
            if (roleID.Equals("3"))//user
            {
                return RedirectToAction("Index", "LearningPage");
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CheckLogin(string name, string password)
        {
            string username = name;
            string userpassword = password;
            if (username == null && userpassword == null)
            {
                TempData["Error"] = "Please enter your user name and password";
                return RedirectToAction("Login", "Account");
            }
            else
            {
                UserDAO obj = new UserDAO();
                User? user = obj.getUser(username, userpassword);
                if (user == null)
                {
                    TempData["Error"] = "User name or Password incorrect";
                    return RedirectToAction("Login", "Account");
                }
                if (user != null)
                {
                    HttpContext.Session.SetString("UserID", user.UserID.ToString());
                    if (user.roleID == "1")//admin
                    {
                        return RedirectToAction("Index", "Admin");
                    }
                    if (user.roleID.Equals("2"))//teacher
                    {
                        return RedirectToAction("OverView", "Teacher");
                    }
                    if (user.roleID.Equals("3"))//user
                    {
                        return RedirectToAction("Index", "LearningPage");
                    }
                }
            }
            return null;
        }
        public IActionResult ChangePassword(string oldPassword,string newPassword, string renewPassword){
            var idUser = HttpContext.Session.GetString("UserID");
            if(oldPassword.Length < 6 || newPassword.Length <6 && renewPassword.Length <6){
                TempData["ErrorLength"]="Password must be more than 6 characters";
                return RedirectToAction("Password","Account");
            }
            else if( newPassword !=renewPassword){
                TempData["ErrorMatch"]="Re-enter incorrect password";
                return RedirectToAction("Password","Account");
            }
            else if(oldPassword== newPassword){
                TempData["ErrorSame"]="The new password must not be the same as the old password";
                return RedirectToAction("Password","Account");
            }
            bool check=dao.ChangePassword(int.Parse(idUser),renewPassword);
            TempData["Success"]="Change password succesfully";
            return RedirectToAction("Password","Account");
        }
        public IActionResult Signup()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateAccount(string username, string fullname, string password, string confirmPassword, string email, string address, string contact)
        {
            try
            {
                UserDAO obj = new UserDAO();
                if (checkValidUser(username, fullname, password, confirmPassword, email, address, contact))
                {
                    return RedirectToAction("Signup", "Account");
                }
                else
                {
                    DateTime today = DateTime.Today;
                    bool check = obj.InsertUser(username, fullname, email, password, address, contact, today);
                    int user_id = obj.GetUserIDByUserName(username);
                    if (check)
                    {
                        obj.insertNoteBookID(user_id);
                        return RedirectToAction("Login", "Account");
                    }
                    else
                    {
                        TempData["Error"] = "Have a error";
                        return RedirectToAction("Signup", "Account");
                    }
                }

            }
            catch (Exception)
            {

                return RedirectToAction("Signup", "Account");
            }

        }
        public bool checkValidUser(string username, string fullname, string password, string confirmPassword, string email, string address, string contact)
        {
            UserDAO obj= new UserDAO();
            bool checkConfirm = checkConformPassword(password, confirmPassword);
            bool checkEmail = Utilities.IsValidEmail(email);
            bool checkDuplicate=obj.getUserName(username);
            if (!checkConfirm)
            {
                TempData["PasswordError"] = "Password not match".ToString();
            }
            if (!checkEmail)
            {
                TempData["EmailError"] = "Invalid email address".ToString();
            }
            if (password.Length < 6)
            {
                TempData["PasswordLengthError"] = "Password too short".ToString();
            }
            if(!checkDuplicate){
                TempData["UserNameError"] = "UserName already exits".ToString();
            }
            if (!checkConfirm || !checkEmail || password.Length < 6 || !checkDuplicate)
            {
                return true;
            }
            return false;
        }
        public bool checkConformPassword(string password, string confirmPassword)
        {
            if (password == confirmPassword)
            {
                return true;
            }
            return false;
        }
        public IActionResult Password(){
            return View();
        }
        public IActionResult PasswordTeacher(){
            return View();
        }
        public IActionResult Logout(){
            HttpContext.Session.Remove("UserID");
            return RedirectToAction("Index", "Home");
        }
        public IActionResult ForgotPassword(){
            return View();
        }
        public IActionResult ComfirmForgotPassword(){
            return View();
        }
        [HttpPost]
        public IActionResult SendEmail(string email){
            User user=dao.getUserbyEmail(email);
            if(user==null){
                TempData["error"]="email is not registered";
                return RedirectToAction("ForgotPassword","Account");
            }
            MailMessage mm = new MailMessage("group3poppin@gmail.com",email);
            mm.Subject="Your password !";
            mm.Body=string.Format("Hello :<h1>{0} is your email id</h1>  <br/> Your password is <h1>{1}</h1>",user.UserEmail,user.UserPassword);
            mm.IsBodyHtml=true;
            SmtpClient smtp= new SmtpClient();
            smtp.Host="smtp.gmail.com";
            smtp.EnableSsl=true;
            NetworkCredential nc= new NetworkCredential();
            nc.UserName="group3poppin@gmail.com";
            nc.Password="group3123";
            smtp.UseDefaultCredentials=false;
            smtp.Credentials=nc;
            smtp.Port=587;
            smtp.Send(mm);
            return View();
        }
    }
}
