﻿using Group3.Models;
using Group3.Models.DAO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Group3.Controllers
{
    public class ReviewingController : Controller
    {
        NotebookDAO notebookDAO = new NotebookDAO();
        Notebook notebook = new Notebook();
        ReviewingDAO reviewingDAO= new ReviewingDAO();
        WordDAO wDao = new WordDAO();
        Word Word =new Word();
        public IActionResult Reviewing(int word_id)
        {
            List<Questions> list = new List<Questions>();
            list = reviewingDAO.GetQuestionsByWordIdInNoteBook(word_id);
            HttpContext.Session.SetString("UserReviewSession", JsonConvert.SerializeObject(list));
            return RedirectToAction("Index");

        }
        public IActionResult Index()
        {
            notebook = notebookDAO.GetNotebookFromUserID(int.Parse(HttpContext.Session.GetString("UserID")));
            //Getting amount of words & the words per level in notebook
            GetQuestionAndAnswer(notebook.notebook_id);
            return View();
        }
        public async Task<IActionResult> GetQuestionAndAnswer(int notebook_id)
        {
            
            List<Questions> questions = new List<Questions>();
            List<Word> wordlist = new List<Word>();
            questions = reviewingDAO.GetQuestionsByWordIdInNoteBook(notebook.notebook_id);
            foreach (var question in questions)
            {
                
                wordlist.Add(wDao.GetWord(reviewingDAO.GetWordIDinNotebook(notebook.notebook_id)));
            }
            
        
            ViewData["WordsForQuestions"] = wordlist;
            ViewData["GetQAforExam"] = questions;
            // ViewData["GetWords"] = wDao.GetWord(word_id);

            return View();
        }
        public IActionResult UpdateWordLevel(int word_id){
            bool check= reviewingDAO.UpdateWordLevelInNotebook(word_id);
            return View();
        }
        public IActionResult Review()
        {
            ViewData["ReviewNotebook"] = JsonConvert.DeserializeObject<List<Word>>(HttpContext.Session.GetString("UserReviewSession"));
            return View();
        }
    }
}
