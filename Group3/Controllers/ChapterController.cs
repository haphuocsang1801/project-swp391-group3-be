using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Group3.Models;
using Group3.Models.DAO;
using System.Web;
using Group3.Helpper;

namespace Group3.Controllers
{
    public class ChapterController : Controller
    {
        ChapterDAO chapterDAO = new ChapterDAO();
        WordDAO wordDAO = new WordDAO();
        CourseDAO courseDAO = new CourseDAO();
        public ChapterController()
        {
        }
        public IActionResult ManageChapterDetail(string chapterID)
        {
            ViewData["Words"]=wordDAO.GetWordsByChapterID(int.Parse(chapterID));
            ViewData["WordTypes"]=wordDAO.GetWordTypes();
            ViewData["chapterID"]=chapterID;
            return View();
        }
        [HttpPost]
        public JsonResult AddQuestionChapter(IFormFile file,string vocabulary,string ipa,string meaning,string type,string ex,int chapterID){
            string? wordtype_id="";
            List<WordType> wordtypeList= wordDAO.GetWordTypes();
            foreach (var item in wordtypeList)
            {
                if(item.wordtype_name==type){
                    wordtype_id=item.wordtype_id;
                }
            }
            var IdTeacher = HttpContext.Session.GetString("UserID");
            Utilities.UpLoadImage(file);
            string fileName="/images/"+file.FileName;
            wordDAO.InsertWord(vocabulary,meaning,ipa,wordtype_id,ex,int.Parse(IdTeacher),fileName);
            int WordID=wordDAO.getIDWordLaster();
            wordDAO.InsertWordChapter(chapterID,WordID);
            List<Word> wordList=wordDAO.GetWordsByChapterID(chapterID);
            var result= new {
                wordList=wordList,wordTypeList=wordtypeList,chapterID=chapterID
            };
            return Json(result);
        }
        [HttpPost]
        public JsonResult UpdateQuestionChapter(IFormFile file,string vocabulary,string ipa,string meaning,string type,string ex,int chapterID,int wordID){
            string? wordtype_id="";
            List<WordType> wordtypeList= wordDAO.GetWordTypes();
            foreach (var item in wordtypeList)
            {
                if(item.wordtype_name==type){
                    wordtype_id=item.wordtype_id;
                    break;
                }
            }
            var IdTeacher = HttpContext.Session.GetString("UserID");
            string fileName=null;
            if(file!=null){
            Utilities.UpLoadImage(file);
            fileName="/images/"+file.FileName;
            }
            wordDAO.UpdateWord(vocabulary,meaning,ipa,wordtype_id,ex,fileName,wordID);
            List<Word> wordList=wordDAO.GetWordsByChapterID(chapterID);
            var result= new {
                wordList=wordList,wordTypeList=wordtypeList,chapterID=chapterID
            };
            return Json(result);
        }
        public JsonResult AddChapter(int idCourse){
            int totalChapterBefore=courseDAO.GetCourseByID(idCourse).course_total_chapters;
            //Add new Chapter
            chapterDAO.AddChaper(totalChapterBefore);
            int chapterID=chapterDAO.getIDChapterLaster();
            chapterDAO.AddChaperWithCourse(chapterID,idCourse);
            int totalChapterAfter=courseDAO.totalCoursebyCourseID(idCourse);
            //Update total chapter of the course after insert new chapter
            courseDAO.UpdateTotalChapter(idCourse,totalChapterAfter);
            List<Chapter> listChapter=chapterDAO.GetChaptersByCourseId(idCourse);
            var result= new {
                list=listChapter,id=idCourse
            };
            return Json(result);
        }
        public JsonResult ChapterDelete(int courseID,int chapterID){
            chapterDAO.DeleteChapterByChapterId(chapterID);
            chapterDAO.DeleteChapter(chapterID);
            List<Chapter> listChapter=chapterDAO.GetChaptersByCourseId(courseID);
            var result= new {
                list=listChapter,id=courseID
            };
            return Json(result);
        }
        public JsonResult DeleteQuesiton(int questionId,int chapterId){
            wordDAO.DeleteQuesiton(questionId);
            wordDAO.DeleteChapterWord(questionId);
            return Json(new{
                redirectUrl = Url.Action("ManageChapterDetail", "Chapter",new {chapterID=chapterId}),
                isRedirect = true 
            });
        }
        public Object getAllOfDataForDetailChapter(int chapterID){
            List<WordType> wordtypeList= wordDAO.GetWordTypes();
            List<Word> wordList=wordDAO.GetWordsByChapterID(chapterID);
            return new {
                wordList=wordList,wordTypeList=wordtypeList,chapterID=chapterID
            };
        }
    }
}