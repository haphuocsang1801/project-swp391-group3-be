using System.Data.SqlClient;
using static Group3.DBContext;

namespace Group3.Models.DAO
{
    public class PaymentDAO
    {
        public int totalCourse(){
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT COUNT(*) FROM dbo.Course";
                sqlCommand.CommandText = query;
                using var reader= sqlCommand.ExecuteReader();
                if(reader!=null){
                    return reader.GetInt16(0);
                }
            }
            catch (System.Exception ex)
            {
            }
            return 0;
        }
    }
}