﻿using System.Data;
using System.Data.SqlClient;
using static Group3.DBContext;
namespace Group3.Models.DAO
{
    public class UserDAO
    {
        public User getUser(string userName, string passsword)
        {
            try
            {
                User obj = new User();
                using SqlCommand sqlComannd = new SqlCommand();
                sqlComannd.Connection = SQL;
                string query = "select * from[User] where username = @username and user_password= @password";
                sqlComannd.CommandText = query;
                sqlComannd.Parameters.AddWithValue("@username", userName);
                sqlComannd.Parameters.AddWithValue("@password", passsword);

                using var reader = sqlComannd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        obj.UserID = reader.GetInt32(0);
                        obj.UserName = reader.GetString(1);
                        obj.FullName = reader.GetString(2);
                        obj.UserEmail = reader.GetString(3);
                        obj.UserPassword = reader.GetString(4);
                        obj.User_Date_register = reader.GetDateTime(5);
                        obj.roleID = reader.GetString(6);
                        obj.urlAvatar = reader.GetString(7);
                        obj.UserAddress = reader.GetString(8);
                        obj.UserContact = reader.GetString(9);
                        obj.SubscriptionId = reader.GetInt32(10);
                    }
                    return obj;
                }
            }
            catch (Exception)
            {
            }
            return null;
        }
        public string getRoleUser(int? UserId)
        {
            try
            {
                string roleID;
                using SqlCommand sqlComannd = new SqlCommand();
                sqlComannd.Connection = SQL;
                string query = "select roleID from [User] where user_id=@userId";
                sqlComannd.CommandText = query;
                sqlComannd.Parameters.AddWithValue("@userId", UserId);
                var reader = sqlComannd.ExecuteScalar();
                if (reader != null)
                {
                    roleID = reader.ToString();
                    return roleID;
                }
            }
            catch (Exception)
            {

            }
            return null;
        }
        public bool InsertUser(string UserName, string FullName, string UserEmail, string Password, string UserAddress, string Contact, DateTime date)

        {
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "INSERT INTO dbo.[User] ( username, user_fullname, user_email, user_password, user_register_date, roleID, user_avatar, user_address, user_contact, subscription_id ) " +
                    "VALUES ( @username,@fullname,@email, @password,@date,'1', '/images/avatar.jpg',@address,@contact,1)";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@username", UserName);
                sqlCommand.Parameters.AddWithValue("@fullname", FullName);
                sqlCommand.Parameters.AddWithValue("@email", UserEmail);
                sqlCommand.Parameters.AddWithValue("@password", Password);
                sqlCommand.Parameters.AddWithValue("@address", UserAddress);
                sqlCommand.Parameters.AddWithValue("@contact", Contact);
                sqlCommand.Parameters.AddWithValue("@date", date);
                var reader = sqlCommand.ExecuteNonQuery();
                if (reader != 0)
                {
                    return true;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return false;
        }
        public bool insertNoteBookeID(int user_id)
        {
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "INSERT INTO dbo.NoteBookUser ( user_id ) VALUES ( @user_id)";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@user_id", user_id);
                var reader = sqlCommand.ExecuteNonQuery();
                if (reader != 0)
                {
                    return true;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return false;
        }
        public bool getUserName(string name){
            try{
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT * FROM dbo.[User] WHERE username='@username'";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@username", name);
                var reader = sqlCommand.ExecuteScalar();
                if(reader!=null){
                    return true;
                }
            }catch(Exception ex){

            }
            finally{
                SqlCommand.close();
            }
            return false;
        }
    }
}
