using System.Data.SqlClient;
using static Group3.DBContext;
namespace Group3.Models.DAO
{
    public class FeedbackDAO
    {
        public List<Feedback> GetFeedback(string value)
        {
            try
            {
                List<Feedback> FeedbackList = new List<Feedback>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                
                string query = "select * from Feedback where Feedback_name LIKE @Feedback_name";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@Feedback_name", "%"+value+"%");
                using var reader = sqlCommand.ExecuteReader();
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        FeedbackList.Add(new Feedback{
                            Feedback_id = reader.GetInt32(0),
                            Feedback_name = reader.GetString(1),
                            Feedback_total_chapters = reader.GetInt32(2),
                            Feedback_create_data = reader.GetDateTime(3),
                            teacher_id = reader.GetInt32(4),
                            Feedback_image = reader.GetString(5)
                        });
                    }
                    return FeedbackList;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return null;
        }
    }
}