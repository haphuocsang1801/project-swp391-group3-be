using System.Data.SqlClient;
using static Group3.DBContext;
namespace Group3.Models.DAO
{
    public static class WaitingDAO
    {
        public static List<WaitingTeacher> getWaitingTeacher(){
            try
            {
                List<WaitingTeacher> listWaiting= new List<WaitingTeacher>();
                using SqlCommand sqlComannd = new SqlCommand();
                sqlComannd.Connection = SQL;
                 string query = "SELECT * FROM dbo.WaitingTeacher WHERE status=0";
                sqlComannd.CommandText = query;
                using var reader = sqlComannd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        listWaiting.Add(new WaitingTeacher{
                            waiting_id=reader.GetInt32(0),
                            user_id=reader.GetInt32(1),
                            status=reader.GetBoolean(2)
                        });
                    }
                    return listWaiting;
            }
            }
            catch (System.Exception ex)
            {
            }
            
            return null;
        }
        public static bool UpdateStatus(int id){
            //UPDATE dbo.[User] SET roleID='2' WHERE user_id
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "UPDATE dbo.WaitingTeacher SET status=1 WHERE user_id=@id ";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@id", id);
                var reader = sqlCommand.ExecuteNonQuery();
                if(reader > 0){
                    return true;
                }
            }
            catch (System.Exception ex)
            {
            }
            return false;
        }
    }
}