﻿using System.Data.SqlClient;
using static Group3.DBContext;
namespace Group3.Models.DAO
{
    public class CourseDAO
    {
        public Course GetCourseByID(int course_id)
        {
            try
            {
                Course course = new Course();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from Course where course_id = @course_id";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@course_id", course_id);
                using var reader = sqlCommand.ExecuteReader();
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        course.course_id = reader.GetInt32(0);
                        course.course_name = reader.GetString(1);
                        course.course_total_chapters = reader.GetInt32(2);
                        course.course_create_data = reader.GetDateTime(3);
                        course.teacher_id = reader.GetInt32(4);
                        course.course_image = reader.GetString(5);
                    }
                    return course;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return null;
        }

        public List<Course> GetCourses()
        {
            try
            {
                List<Course> CourseList= new List<Course>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from Course ";
                sqlCommand.CommandText = query;
                using var reader = sqlCommand.ExecuteReader();
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        CourseList.Add(new Course
                        {
                            course_id = reader.GetInt32(0),
                            course_name = reader.GetString(1),
                            course_total_chapters = reader.GetInt32(2),
                            course_create_data = reader.GetDateTime(3),
                            teacher_id = reader.GetInt32(4),
                            course_image = reader.GetString(5)
                        });
                    }
                    return CourseList;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return null;
        }
        public int totalCourse(){
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT COUNT(*) FROM dbo.Course";
                sqlCommand.CommandText = query;
                using var reader= sqlCommand.ExecuteReader();
                if(reader!=null){
                    return reader.GetInt16(0);
                }
            }
            catch (System.Exception ex)
            {
            }
            return 0;
        }
    }
}
