using System.Data.SqlClient;
using static Group3.DBContext;

namespace Group3.Models.DAO
{
    public class PaymentDAO
    {
        public int totalPayment(){
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT COUNT(*) FROM dbo.Payment";
                sqlCommand.CommandText = query;
                var reader= sqlCommand.ExecuteScalar();
                if(reader!=null){
                    return (int) reader;
                }
            }
            catch (Exception ex)
            {
            }
            return 0;
        }
        public List<Payment> getListUserPayment(){
            try
            {
                List<Payment> list = new List<Payment>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT * FROM dbo.Payment";
                sqlCommand.CommandText = query;
                using var reader= sqlCommand.ExecuteReader();
                if(reader != null){
                    while(reader.Read()){
                        list.Add(new Payment{
                            payment_id=reader.GetInt32(0),
                            method_id=reader.GetInt32(1),
                            payment_date= (reader.GetDateTime(2)).Date,
                            user_id= reader.GetInt32(3)
                        });
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {}
            return null;
        }
    }
}