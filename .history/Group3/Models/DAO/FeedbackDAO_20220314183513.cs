using System.Data.SqlClient;
using static Group3.DBContext;

namespace Group3.Models.DAO
{
    public static class FeedbackDAO
    {
        public static bool InsertFeedBack(string content,int id,int? recipient_id){
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "INSERT INTO dbo.Feedback(feedback_time,feedback_content,user_id,status,recipient_id) VALUES (GETDATE(),@content,@id,0,@recipient_id) ";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@content", content);
                sqlCommand.Parameters.AddWithValue("@id", id);
                sqlCommand.Parameters.AddWithValue("@recipient_id", recipient_id);
                var reader = sqlCommand.ExecuteNonQuery();
                if(reader > 0){
                    return true;
                }
            }
            catch (System.Exception ex)
            {
            }
            return false;
        }
    }
}