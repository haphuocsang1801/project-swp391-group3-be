using System.Data.SqlClient;
using static Group3.DBContext;

namespace Group3.Models.DAO
{
    public static class TeacherDAO
    {
        public static List<Teacher> getTeacherList(){
            try
            {
                List<Teacher> TeacherList= new List<Teacher>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from Teacher ";
                sqlCommand.CommandText = query;
                using var reader = sqlCommand.ExecuteReader();
                if(reader!=null){
                    while(reader.Read()){
                        TeacherList.Add(new Teacher{
                            user_id=reader.GetInt32(0),
                            certificate=reader.GetString(1)
                        });
                    }
                    return TeacherList;
                }
            }
            catch (System.Exception ex)
            {
            }

            return null;
        }
    }
}