using System.Data.SqlClient;
using static Group3.DBContext;

namespace Group3.Models.DAO
{
    public static class TeacherDAO
    {
        public static List<Teacher> getTeacherList(){
            try
            {
                List<Teacher> TeacherList= new List<Teacher>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "SELECT * FROM dbo.Teacher";
                sqlCommand.CommandText = query;
                using var reader = sqlCommand.ExecuteReader();
                if(reader!=null){
                    while(reader.Read()){
                        TeacherList.Add(new Teacher{
                            user_id=reader.GetInt32(0),
                            certificate=reader.GetString(1),
                            isActive=reader.GetBoolean(2)
                        });
                    }
                    return TeacherList;
                }
            }
            catch (System.Exception ex)
            {
            }

            return null;
        }
        public static Teacher GetTeacher(int id){
            try
            {
                Teacher teacher= new Teacher();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from Teacher where user_id=@id ";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@id", id);
                using var reader = sqlCommand.ExecuteReader();
                if(reader!=null){
                    while(reader.Read()){
                        teacher.user_id=reader.GetInt32(0);
                        teacher.certificate=reader.GetString(1);    
                        teacher.isActive=reader.GetBoolean(2);
                    }
                }
                return teacher;
            }
            catch (System.Exception ex)
            {
            }

            return null;
        }
        public static Teacher GetTeacherSearch(string value){
            try
            {
                Teacher teacher= new Teacher();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "select * from Teacher where user_id=@id ";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@id", id);
                using var reader = sqlCommand.ExecuteReader();
                if(reader!=null){
                    while(reader.Read()){
                        teacher.user_id=reader.GetInt32(0);
                        teacher.certificate=reader.GetString(1);    
                        teacher.isActive=reader.GetBoolean(2);
                    }
                }
                return teacher;
            }
            catch (System.Exception ex)
            {
            }

            return null;
        }
        public static bool UpdateRoleUserToTeacher(int id){
            //UPDATE dbo.[User] SET roleID='2' WHERE user_id
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "UPDATE dbo.[User] SET roleID='2' WHERE user_id=@id ";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@id", id);
                var reader = sqlCommand.ExecuteNonQuery();
                if(reader > 0){
                    return true;
                }
            }
            catch (System.Exception ex)
            {
            }

            return false;
        }
        public static bool UpdateIsActiveTeacher(int id){
            //UPDATE dbo.[User] SET roleID='2' WHERE user_id
            try
            {
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                string query = "UPDATE dbo.Teacher SET isActive=1 WHERE user_id= @id ";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@id", id);
                var reader = sqlCommand.ExecuteNonQuery();
                if(reader > 0){
                    return true;
                }
            }
            catch (System.Exception ex)
            {
            }

            return false;
        }
    }
}