using System.Data.SqlClient;
using static Group3.DBContext;
namespace Group3.Models.DAO
{
    public class FeedbackDAO
    {
        public List<Course> GetCourseByIDandName(string name)
        {
            try
            {
                List<Course> courseList = new List<Course>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                //select * from Course where course_id = @course_id OR course_name LIKE '%@course_name%'
                string query = "select * from Course where course_name LIKE @course_name";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@course_name", "%"+name+"%");
                using var reader = sqlCommand.ExecuteReader();
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        courseList.Add(new Course{
                            course_id = reader.GetInt32(0),
                            course_name = reader.GetString(1),
                            course_total_chapters = reader.GetInt32(2),
                            course_create_data = reader.GetDateTime(3),
                            teacher_id = reader.GetInt32(4),
                            course_image = reader.GetString(5)
                        });
                    }
                    return courseList;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return null;
        }
    }
}