using System.Data.SqlClient;
using static Group3.DBContext;
namespace Group3.Models.DAO
{
    public static class SubscriptionDAO
    {
        public static List<Subscription> getSubscriptionList(){
            
            try
            {
                List<Subscription> list = new List<Subscription>();
                using SqlCommand sqlComannd = new SqlCommand();
                sqlComannd.Connection = SQL;
                string query = "SELECT * FROM dbo.[Subscription]";
                sqlComannd.CommandText = query;
                using var reader = sqlComannd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        
                        list.Add(new Subscription{
                            Subscription_id=reader.GetInt16(0),
                            Subscription_name=reader.GetFloat(1),
                            Subscription_type=reader.GetString(2)
                        });
                    }
                    return list;
                }
            }
            catch (System.Exception ex)
            {
            }
            return null;
        }
    }
}