using System.Data.SqlClient;
using static Group3.DBContext;
namespace Group3.Models.DAO
{
    public static class FeeDAO
    {
        public static List<Fee> getFeeList(){
            
            try
            {
                List<Fee> list = new List<Fee>();
                using SqlCommand sqlComannd = new SqlCommand();
                sqlComannd.Connection = SQL;
                string query = "SELECT * FROM dbo.[Fee]";
                sqlComannd.CommandText = query;
                using var reader = sqlComannd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        
                        list.Add(new Fee{
                            fee_id=reader.GetInt16(0),
                            fee_name=1.1,
                            fee_type=reader.GetString(2)
                        });
                    }
                    return list;
                }
            }
            catch (System.Exception ex)
            {
            }
            return null;
        }
    }
}