using System.Data.SqlClient;
using static Group3.DBContext;
namespace Group3.Models.DAO
{
    public static class SubscriptionDAO
    {
        public static List<Subscription> getSubscriptionList(){
            
            try
            {
                List<Subscription> listSubscription = new List<Subscription>();
                using SqlCommand sqlComannd = new SqlCommand();
                sqlComannd.Connection = SQL;
                string query = "SELECT * FROM dbo.[Subscription]";
                sqlComannd.CommandText = query;
                using var reader = sqlComannd.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        
                        listSubscription.Add(new Subscription{
                            subscription_id=reader.GetInt16(0),
                            fee_id=reader.GetInt16(1),
                            date_subscription=reader.GetDateTime(2),
                            date_expiration=reader.GetDateTime(3),
                        });
                    }
                    return listSubscription;
                }
            }
            catch (System.Exception ex)
            {
            }
            return null;
        }
    }
}