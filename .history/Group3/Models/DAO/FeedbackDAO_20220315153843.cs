using System.Data.SqlClient;
using static Group3.DBContext;
namespace Group3.Models.DAO
{
    public static class FeedbackDAO
    {
        public static List<Feedback> GetFeedback(string value)
        {
            try
            {
                List<Feedback> FeedbackList = new List<Feedback>();
                using SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = SQL;
                
                string query = "select * from Feedback where feedback_content LIKE @Feedback_name";
                sqlCommand.CommandText = query;
                sqlCommand.Parameters.AddWithValue("@Feedback_name", "%"+value+"%");
                using var reader = sqlCommand.ExecuteReader();
                if (reader != null)
                {
                    while (reader.Read())
                    {
                        FeedbackList.Add(new Feedback{
                            feedback_id=reader.GetInt32(0),
                            feedback_time=reader.GetDateTime(1),
                            feedback_content=reader.GetString(2),
                            user_id=reader.GetInt32(3),
                            status=reader.GetBoolean(4),
                            recipient_id=reader.GetInt32(5)
                        });
                    }
                    return FeedbackList;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return null;
        }
    }
}