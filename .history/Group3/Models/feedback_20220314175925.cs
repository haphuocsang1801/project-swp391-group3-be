namespace Group3.Models
{
    public class Feedback
    {
        public int feedback_id { get; set; }
        
        public DateTime feedback_time { get; set; }
        
        public string feedback_content { get; set; }
        
        public int user_id { get; set; }
        
        public bool status { get; set; }
        public int recipient_id { get; set; }
        
        
        
        
    }
}