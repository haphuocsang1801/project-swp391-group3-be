namespace Group3.Models
{
    public class Payment
    {
        public int payment_id { get; set; }
        public int? method_id { get; set; }
        public DateOnly payment_date { get; set; }
        public int user_id { get; set; }
    }
}