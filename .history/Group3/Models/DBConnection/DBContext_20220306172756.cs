﻿
using System.Data.SqlClient;

namespace Group3
{
    public  class DBContext {
        public static SqlConnection? SQL;
        public static void Connection(string connection)
        {
            try
            {
                SQL = new SqlConnection(connection);
                SQL.Open();
                Console.WriteLine(SQL.State);
            }
            catch (Exception)
            {
                Console.Write("Connection faild");            }
        }
        public static void CloseConnection()
        {
            try
            {
                SQL.Close();
            }
            catch (Exception)
            {
                Console.WriteLine("Close fail");
            }
        }
    }
}
