namespace Group3.Models
{
    public class Payment
    {
        public int payment_id { get; set; }
        public string payment_method { get; set; }
        
        public DateTime payment_date { get; set; }
        
        public int user_id { get; set; }
        
        
        
    }
}