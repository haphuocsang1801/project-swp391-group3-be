﻿namespace Group3.Models
{
    public class Course
    {
        public int course_id { get; set; }
        public string course_name { get; set;}
        public int course_total_chapters { get; set; }
        public DateTime course_create_data { get; set; }
        public int teacher_id { get; set; }
        public string course_image { get; set; }

    }
}
