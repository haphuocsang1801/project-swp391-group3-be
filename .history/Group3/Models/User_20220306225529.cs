﻿namespace Group3.Models
{
    public class User
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string UserEmail { get; set; }
        public string UserPassword { get; set; }
        public DateTime User_Date_register { get; set; }
        public string roleID { get; set; }
        public string urlAvatar { get; set; }
        public string? UserAddress { get; set; }
        public string? UserContact { get; set; }
        public int SubscriptionId { get; set; }

        public int NoteBookId { get; set; }
    }
}
