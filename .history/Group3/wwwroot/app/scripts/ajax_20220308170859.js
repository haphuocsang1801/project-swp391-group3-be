function searchRealTime() {}
$(document).ready(function () {
    $("#SearchCourse").keyup(function () {
        let SearchValue = $("#SearchCourse").val();
        let SetData = $("#DataSearching");
        SetData.html("");
        $.ajax({
            type: "post",
            url: "GetCourseAdmin?SearchValue=" + SearchValue,
            contentType: "html",
            success: function (result) {},
        });
    });
});
function searchCourse(result) {
    if (result.length == 0) {
        SetData.append("<tr><td>Not Found</td></tr>");
    } else {
        result.courses.forEach((item) => {
            let author;
            result.users.forEach((elem) => {
                if (elem.userID == item.teacher_id) {
                    author = elem.fullName;
                }
            });
            let dateText = item.course_create_data;
            let dateOnly = dateText.split("T");
            let Data = `
            <tr>
                <td>
                    <div class="admin-table-number">${item.course_id}</div>
                </td>
                <td>
                    <div class="admin-table-name">
                        <img
                            class="admin-table-image"
                            src="${item.course_image}"
                        />
                        <p class="admin-table-name">${item.course_name}</p>
                    </div>
                </td>
                <td>
                    <div class="admin-table-type">${author}</div>
                </td>
                <td style="text-align:center" >
                    <div class="admin-table-type">${dateOnly[0]}</div>
                </td>
                <td style="text-align:center">
                    <div class="admin-table-type">${item.course_total_chapters}</div>
                </td>
            </tr>`;
            SetData.append(Data);
        });
    }
}
