$(document).ready(function () {
    $("#SearchCourse").keyup(function () {
        let SearchValue = $("#SearchCourse").val();
        let SetData = $("#DataSearching");
        SetData.html("");
        $.ajax({
            type: "post",
            url: "GetCourseAdmin?SearchValue=" + SearchValue,
            contentType: "html",
            success: function (result) {
                console.log(result);
                if (result.length == 0) {
                    SetData.append("<tr><td>Not Found</td></tr>");
                } else {
                    result.courses.forEach((item) => {
                        let author;
                        result.users.forEach((elem) => {
                            if (elem.userID == item.teacher_id) {
                                author = elem.fullName;
                            }
                        });
                        let dateText = item.course_create_data;
                        let dateOnly = dateText.split("T");
                        let Data = `
                        <tr>
                            <td>
                                <div class="admin-table-number">${item.course_id}</div>
                            </td>
                            <td>
                                <div class="admin-table-name">
                                    <img
                                        class="admin-table-image"
                                        src="${item.course_image}"
                                    />
                                    <p class="admin-table-name">${item.course_name}</p>
                                </div>
                            </td>
                            <td>
                                <div class="admin-table-type">${author}</div>
                            </td>
                            <td style="text-align:center" >
                                <div class="admin-table-type">${dateOnly[0]}</div>
                            </td>
                            <td style="text-align:center">
                                <div class="admin-table-type">${item.course_total_chapters}</div>
                            </td>
                        </tr>`;
                        SetData.append(Data);
                    });
                }
            },
        });
    });
});
//
//-------------------------------------------------------------------------
$(document).ready(function () {
    $("#SearchUser").keyup(function () {
        let Value = $("#SearchUser").val();
        let SetData = $("#DataUsers");
        SetData.html("");
        $.ajax({
            type: "post",
            url: "GetUsersAdmin?Value=" + Value,
            contentType: "html",
            success: function (result) {
                if (result.length == 0) {
                    SetData.append("<tr><td>Not Found</td></tr>");
                } else {
                    console.log(result);
                    result.users.forEach((item) => {
                        if (item.roleID == "3" || item.roleID == "4") {
                            let feeType = ``;

                            result.subscriptions.forEach((sub) => {
                                if (
                                    sub.subscription_id == item.SubscriptionId
                                ) {
                                    result.fees.forEach((fee) => {
                                        if (fee.fee_id == sub.fee_id) {
                                            if (fee.fee_id == 1) {
                                                feeType = ` <div class="admin-table-action admin-table-action--purple">
                                                    ${fee.fee_type}
                                                </div>`;
                                            }
                                            if (fee.fee_id == 2) {
                                                feeType = ` <div class="admin-table-action admin-table-action--orange">
                                                    ${fee.fee_type}
                                                </div>`;
                                            }
                                            if (fee.fee_id == 3) {
                                                feeType = ` <div class="admin-table-action admin-table-action--green">
                                                    ${fee.fee_type}
                                                </div>`;
                                            }
                                        }
                                    });
                                }
                            });
                            let Data = ``;
                        }
                    });
                    // result.courses.forEach((item) => {
                    //     let author;
                    //     result.users.forEach((elem) => {
                    //         if (elem.userID == item.teacher_id) {
                    //             author = elem.fullName;
                    //         }
                    //     });
                    //     let dateText = item.course_create_data;
                    //     let dateOnly = dateText.split("T");
                    //     let Data = `
                    //     <tr>
                    //         <td>
                    //             <div class="admin-table-number">${item.course_id}</div>
                    //         </td>
                    //         <td>
                    //             <div class="admin-table-name">
                    //                 <img
                    //                     class="admin-table-image"
                    //                     src="${item.course_image}"
                    //                 />
                    //                 <p class="admin-table-name">${item.course_name}</p>
                    //             </div>
                    //         </td>
                    //         <td>
                    //             <div class="admin-table-type">${author}</div>
                    //         </td>
                    //         <td style="text-align:center" >
                    //             <div class="admin-table-type">${dateOnly[0]}</div>
                    //         </td>
                    //         <td style="text-align:center">
                    //             <div class="admin-table-type">${item.course_total_chapters}</div>
                    //         </td>
                    //     </tr>`;
                    //     SetData.append(Data);
                    // });
                }
            },
        });
    });
});
