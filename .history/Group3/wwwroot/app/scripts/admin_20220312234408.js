//chart
const myChartSale = document.querySelector("#myChart");
if (myChartSale) {
    handleChart(myChartSale);
}
const myChartQuiz = document.querySelector("#myChartQuizzes");
if (myChartQuiz) {
    handleMutipleChart(myChartQuiz);
}
function handleChart(chart) {
    const labels = ["January", "February", "March", "April", "May", "June"];
    const data = {
        labels: labels,
        datasets: [
            {
                label: "Revenue",
                backgroundColor: "rgb(255, 99, 132)",
                borderColor: "rgb(255, 99, 132)",
                data: [0, 10, 5, 2, 20, 100, 200],
            },
        ],
    };
    const config = {
        type: "line",
        data: data,
        options: {},
    };
    const myChart = new Chart(chart, config);
}
function handleMutipleChart(chart) {
    const labels = ["January", "February", "March", "April", "May", "June"];
    const data = {
        labels: labels,
        datasets: [
            {
                type: "line",
                label: "Not Pass",
                backgroundColor: "rgb(255, 99, 132)",
                borderColor: "rgb(255, 99, 132)",
                data: [0, 10, 5, 2, 20, 100, 200],
            },
            {
                type: "line",
                label: "Pass",
                backgroundColor: "rgb(117, 206, 85)",
                borderColor: "rgb(117, 206, 85)",
                data: [0, 20, 15, 31, 21, 180, 200],
            },
        ],
    };
    const config = {
        type: "scatter",
        data: data,
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                },
            },
        },
    };
    const myChart = new Chart(chart, config);
}

//----------------------------------------------------------------------------

//feature-click
const featureItems = document.querySelectorAll(".admin-feature-item ");
[...featureItems].forEach((item) => {
    item.addEventListener("click", function (e) {
        [...featureItems].forEach((elem) => {
            elem.classList.remove("feature-click");
        });
        item.classList.add("feature-click");
    });
});

// const profile = document.querySelector(".profile");
// const template = `
// <div class="modal">
// <div class="modal-main">
//   <div class="modal-top">
//     <div class="modal-title">Profile </div>
//     <i class="far fa-times modal-close"></i>
//   </div>
//   <div class="modal-bottom"> <img class="modal-image" src="./images/Course.png" alt="">
//     <div class="modal-profile">
//       <div class="modal-profile-double">
//         <div class="modal-profile-input">
//           <h3 class="modal-input-name">Last Name</h3>
//           <div class="modal-input-value">Sang</div>
//         </div>
//         <div class="modal-profile-input">
//           <h3 class="modal-input-name">First Name</h3>
//           <div class="modal-input-value">Ha</div>
//         </div>
//       </div>
//       <div class="modal-profile-input">
//         <h3 class="modal-input-name">Email</h3>
//         <div class="modal-input-value">haphuocsang1801@gmail.com</div>
//       </div>
//       <div class="modal-profile-input">
//         <h3 class="modal-input-name">Contact number</h3>
//         <div class="modal-input-value">0795911712</div>
//       </div>
//       <div class="modal-profile-input">
//         <h3 class="modal-input-name">Address</h3>
//         <div class="modal-input-value">CanTho city</div>
//       </div>
//       <div class="modal-profile-input">
//         <h3 class="modal-input-name">Certificate url</h3><a class="modal-input-value modal-textdecor" href="null">https://www.coursera.org/account/accomplis.....</a>
//       </div>
//     </div>
//   </div>
//   <div class="modal-submit">
//     <button class="button-base">Approve</button>
//     <button class="button-base button-base--red">Reject</button>
//   </div>
// </div>
// </div>`;
// if (profile) {
//     profile.addEventListener("click", function (e) {
//         document.body.insertAdjacentHTML("beforeend", template);
//     });
// }
// document.body.addEventListener("click", handleClose);
// function handleClose(e) {
//     if (e.target.matches(".modal-close")) {
//         const modal = e.target.parentNode.parentNode.parentNode;
//         modal.parentNode.removeChild(modal);
//     } else if (e.target.matches(".modal")) {
//         e.target.parentNode.removeChild(e.target);
//     }
// }
