//chart
const myChartSale = document.querySelector("#myChart");
if (myChartSale) {
    handleChart(myChartSale);
}
const myChartQuiz = document.querySelector("#myChartQuizzes");
if (myChartQuiz) {
    handleMutipleChart(myChartQuiz);
}
function handleChart(chart) {
    const labels = ["January", "February", "March", "April", "May", "June"];
    const data = {
        labels: labels,
        datasets: [
            {
                label: "Revenue",
                backgroundColor: "rgb(255, 99, 132)",
                borderColor: "rgb(255, 99, 132)",
                data: [0, 10, 50, 29, 100, 200, 200],
                tension: 1,
            },
        ],
    };
    const config = {
        type: "line",
        data: data,
        options: {},
    };
    const myChart = new Chart(chart, config);
}
function handleMutipleChart(chart) {
    const labels = ["January", "February", "March", "April", "May", "June"];
    const data = {
        labels: labels,
        datasets: [
            {
                type: "line",
                label: "Not Pass",
                backgroundColor: "rgb(255, 99, 132)",
                borderColor: "rgb(255, 99, 132)",
                data: [0, 10, 5, 2, 20, 100, 200],
            },
            {
                type: "line",
                label: "Pass",
                backgroundColor: "rgb(117, 206, 85)",
                borderColor: "rgb(117, 206, 85)",
                data: [0, 20, 15, 31, 21, 180, 200],
            },
        ],
    };
    const config = {
        type: "scatter",
        data: data,
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                },
            },
        },
    };
    const myChart = new Chart(chart, config);
}

//----------------------------------------------------------------------------

//feature-click
const featureItems = document.querySelectorAll(".admin-feature-item ");
[...featureItems].forEach((item) => {
    item.addEventListener("click", function (e) {
        [...featureItems].forEach((elem) => {
            elem.classList.remove("feature-click");
        });
        item.classList.add("feature-click");
    });
});
