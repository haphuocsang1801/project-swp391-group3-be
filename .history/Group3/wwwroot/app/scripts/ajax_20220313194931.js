$(document).ready(function () {
    $("#SearchCourse").keyup(function () {
        let SearchValue = $("#SearchCourse").val();
        let SetData = $("#DataSearching");
        SetData.html("");
        $.ajax({
            type: "post",
            url: "GetCourseAdmin?SearchValue=" + SearchValue,
            contentType: "html",
            success: function (result) {
                console.log(result);
                if (result.length == 0) {
                    SetData.append("<tr><td>Not Found</td></tr>");
                } else {
                    result.courses.forEach((item) => {
                        let author;
                        result.users.forEach((elem) => {
                            if (elem.userID == item.teacher_id) {
                                author = elem.fullName;
                            }
                        });
                        let dateText = item.course_create_data;
                        let dateOnly = dateText.split("T");
                        let Data = `
                        <tr>
                            <td>
                                <div class="admin-table-number">${item.course_id}</div>
                            </td>
                            <td>
                                <div class="admin-table-name">
                                    <img
                                        class="admin-table-image"
                                        src="${item.course_image}"
                                    />
                                    <p class="admin-table-name">${item.course_name}</p>
                                </div>
                            </td>
                            <td>
                                <div class="admin-table-type">${author}</div>
                            </td>
                            <td style="text-align:center" >
                                <div class="admin-table-type">${dateOnly[0]}</div>
                            </td>
                            <td style="text-align:center">
                                <div class="admin-table-type">${item.course_total_chapters}</div>
                            </td>
                        </tr>`;
                        SetData.append(Data);
                    });
                }
            },
        });
    });
});
//-------------------------------------------------------------------------
//search user
$(document).ready(function () {
    $("#SearchUser").keyup(function () {
        let Value = $("#SearchUser").val();
        let SetData = $("#DataUsers");
        SetData.html("");
        $.ajax({
            type: "post",
            url: "GetUsersAdmin?Value=" + Value,
            contentType: "html",
            success: function (result) {
                if (result.length == 0) {
                    SetData.append("<tr><td>Not Found</td></tr>");
                } else {
                    console.log(result);
                    result.users.forEach((item) => {
                        if (item.roleID == "3" || item.roleID == "4") {
                            let feeType = ``;
                            result.subscriptions.forEach((sub) => {
                                if (
                                    sub.subscription_id == item.subscriptionId
                                ) {
                                    result.fees.forEach((fee) => {
                                        if (fee.fee_id == sub.fee_id) {
                                            if (fee.fee_id == 1) {
                                                feeType = ` <div class="admin-table-action admin-table-action--purple">
                                                                ${fee.fee_type}
                                                            </div>`;
                                            }
                                            if (fee.fee_id == 2) {
                                                feeType = ` <div class="admin-table-action admin-table-action--orange">
                                                    ${fee.fee_type}
                                                </div>`;
                                            }
                                            if (fee.fee_id == 3) {
                                                feeType = ` <div class="admin-table-action admin-table-action--green">
                                                    ${fee.fee_type}
                                                </div>`;
                                            }
                                        }
                                    });
                                }
                            });
                            let Data = `
                            <tr>
                                <td>
                                    <div class="admin-table-number">
                                        ${item.userID}
                                    </div>
                                </td>
                                <td>
                                    <div class="admin-table-name">
                                        <img
                                            class="admin-table-image"
                                            src="${item.urlAvatar}"
                                            alt=""
                                        />
                                        <p class="admin-table-name">
                                            ${item.fullName}
                                        </p>
                                    </div>
                                </td>
                                <td>
                                    <div class="admin-table-type">
                                        ${item.userEmail}
                                    </div>
                                </td>
                                <td>
                                    ${feeType}
                                </td>
                                <td>
                                ${
                                    item.roleID == "3"
                                        ? `
                                        <div class="admin-table-action admin-table-action--green">
                                            Active
                                        </div>`
                                        : `
                                        <div class="admin-table-action admin-table-action--none">
                                            None
                                        </div>`
                                }
                                </td>
                                <td class="center">
                                    <a
                                        class="admin-table-action admin-table-action--red"
                                        asp-controller="User" asp-action="UpdateRole" asp-route-id="${
                                            item.UserID
                                        }"
                                        ><i class="fas fa-trash"></i
                                    ></a>
                                </td>
                            </tr>
                            `;
                            SetData.append(Data);
                        }
                    });
                }
            },
        });
    });
});
//handle detail infomation teacher
$(document).ready(function () {
    const detaiList = document.querySelectorAll("#detail-user");
    detaiList.forEach((item) => {
        item.addEventListener("click", function () {
            let Value = item.getAttribute("value");
            $.ajax({
                type: "post",
                url: "TeacherAdminJson?Value=" + Value,
                contentType: "html",
                success: function (result) {
                    result.userList.forEach((element) => {
                        if (result.teacher.user_id == element.userID) {
                            let template = `
                                <div class="modal">
                                <div class="modal-main"> 
                                    <div class="modal-top"> 
                                    <div class="modal-title">Profile </div>
                                    <i class="far fa-times modal-close"></i>
                                    </div>
                                    <div class="modal-bottom"> <img class="modal-image" src="${element.urlAvatar}" alt="">
                                    <div class="modal-profile"> 
                                        <div class="modal-profile-double"> 
                                        <div class="modal-profile-input"> 
                                            <h3 class="modal-input-name">Last Name</h3>
                                            <div class="modal-input-value">Sang</div>
                                        </div>
                                        <div class="modal-profile-input"> 
                                            <h3 class="modal-input-name">First Name</h3>
                                            <div class="modal-input-value">Ha</div>
                                        </div>
                                        </div>
                                        <div class="modal-profile-input"> 
                                        <h3 class="modal-input-name">Email</h3>
                                        <div class="modal-input-value">${element.userEmail}</div>
                                        </div>
                                        <div class="modal-profile-input"> 
                                        <h3 class="modal-input-name">Contact number</h3>
                                        <div class="modal-input-value">${element.userContact}</div>
                                        </div>
                                        <div class="modal-profile-input"> 
                                        <h3 class="modal-input-name">Address</h3>
                                        <div class="modal-input-value">${element.userAddress}</div>
                                        </div>
                                        <div class="modal-profile-input"> 
                                        <h3 class="modal-input-name">Certificate url</h3><a class="modal-input-value modal-textdecor" href="null">${result.teacher.certificate}</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="modal-submit"> 
                                    <button class="button-base" id="btn-detail"   onclick="handleApprove(this)" value="Approve" data-id="${result.teacher.user_id}">Approve</button>
                                    <button class="button-base button-base--red" id="btn-detail" onclick="handleApprove(this)"value="Reject" data-id="${result.teacher.user_id}">Reject</button>
                                    </div>
                                </div>
                                </div>`;
                            document.body.insertAdjacentHTML(
                                "beforeend",
                                template
                            );
                            document.body.addEventListener(
                                "click",
                                handleClose
                            );
                            function handleClose(e) {
                                if (e.target.matches(".modal-close")) {
                                    const modal =
                                        e.target.parentNode.parentNode
                                            .parentNode;
                                    modal.parentNode.removeChild(modal);
                                } else if (e.target.matches(".modal")) {
                                    e.target.parentNode.removeChild(e.target);
                                }
                            }
                        }
                    });
                },
            });
        });
    });
});
//handle reject or accept
function handleApprove(e) {
    let value = e.getAttribute("value");
    let userID = e.getAttribute("data-id");
    handleAjax(
        "post",
        "ApproveTeacher?Value=" + value + "&ID=" + userID,
        handleResultAjax
    );
}

//ajax commom to use
function handleAjax(method, url, handleResult) {
    $.ajax({
        type: method,
        url: url,
        contentType: "html",
        success: handleResult,
    });
}
function handleResultAjax(result) {
    if (result.isRedirect) {
        window.location.href = result.redirectUrl;
    }
}
// handle search in teacher
function handleSearchTeacher(this) {
    console.log(this);
    let searchTeacher = document.querySelector("#valueTeacher");
    searchTeacher.addEventListener("keyup", function (e) {
        let valueTeacher = e.target.value;
        handleAjax(
            "post",
            "GetTeacherAdmin?Value=" + valueTeacher,
            handleSearchTeacherJson
        );
    });
}
function handleSearchTeacherJson(result) {
    let template=""
}
