$(document).ready(function () {
    $("#SearchCourse").keyup(function () {
        let SearchValue = $("#SearchCourse").val();
        let SetData = $("#DataSearching");
        SetData.html("");
        $.ajax({
            type: "post",
            url: "GetCourseAdmin?SearchValue=" + SearchValue,
            contentType: "html",
            success: function (result) {
                console.log(result);
                if (result.length == 0) {
                    SetData.append("<tr><td>Not Found</td></tr>");
                } else {
                    $.each(result, function (index, value) {
                        let dateText = value.course_create_data;
                        let dateOnly = dateText.split("T");
                        let Data = `
                        <tr>
                            <td>
                                <div class="admin-table-number">${value.course_id}</div>
                            </td>
                            <td>
                                <div class="admin-table-name">
                                    <img
                                        class="admin-table-image"
                                        src="${value.course_image}"
                                    />
                                    <p class="admin-table-name">${value.course_name}</p>
                                </div>
                            </td>
                            <td>
                                <div class="admin-table-type">Ronaldo</div>
                            </td>
                            <td>
                                <div class="admin-table-type">${dateOnly[0]}</div>
                            </td>
                            <td>
                                <div class="admin-table-type">${value.course_total_chapters}</div>
                            </td>
                        </tr>`;
                        SetData.append(Data);
                    });
                }
            },
        });
    });
});
