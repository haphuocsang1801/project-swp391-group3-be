﻿using Microsoft.AspNetCore.Mvc;
using Group3.Models.DAO;
using Group3.Models;
namespace Group3.Controllers
{
    public class UserController : Controller
    {
        UserDAO dao = new UserDAO();
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult UserAdmin(){
            List<User> Userlist =  dao.getListUser();
            List<Fee> FeeList=FeeDAO.getFeeList();
            ViewData["Users"]=Userlist;
            ViewData["Fees"]=FeeList;
            ViewData["Subscriptions"]=SubscriptionDAO.getSubscriptionList();
            return View();
        }
        [HttpGet]
        [Route("/User/UpdateRole/{id}")]
        public IActionResult UpdateRole(string id){
            if(dao.getRoleUser(Int16.Parse(id))=="3"){
                if(dao.UpdateRoleIDByID(Int16.Parse(id),"4")){
                    return RedirectToAction("UserAdmin", "User");
                }
            }
            else{
                if(dao.UpdateRoleIDByID(Int16.Parse(id),"3")){
                    return RedirectToAction("UserAdmin", "User");
                }
            }
            
            return RedirectToAction("UserAdmin", "User");
        }
        public JsonResult GetUserAdmin(string SearchValue){
            List<Course> CourseList=new List<Course>();
            CourseList=dao.GetCourseByIDandName(SearchValue);
            List<User> UserList= new UserDAO().getListUser();
            ViewModelCourseUser obj= new ViewModelCourseUser();
            obj.Users=UserList;
            obj.Courses=CourseList;
            return Json(obj);
        }
    }
    public class ViewModelUserFeesSubscriptions
        {
            public IEnumerable<User> Users { get; set; }
            public IEnumerable<Fee> Fees { get; set; }
        }
}
