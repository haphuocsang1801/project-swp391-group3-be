﻿using Microsoft.AspNetCore.Mvc;
using Group3;
namespace Group3.Controllers
{
    public class UserController : Controller
    {
        UserDAO dao = new UserDAO();
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult UserAdmin(){
            List<User> list =  dao.getListUser();
            return View();
        }
    }
}
