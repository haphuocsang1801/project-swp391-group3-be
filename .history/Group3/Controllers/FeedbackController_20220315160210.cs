using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Group3.Models.DAO;
//using Group3.Models;

namespace Group3.Controllers
{
    public class FeedbackController : Controller
    {
        UserDAO dao= new UserDAO();
        public FeedbackController()
        {
        }

        public IActionResult Index()
        {
            ViewData["Feedbacks"]=FeedbackDAO.GetFeedback("");
            List<User> UserList=new List<User>();
            UserList=dao.getListUser();
            return View();
        }
    }
}