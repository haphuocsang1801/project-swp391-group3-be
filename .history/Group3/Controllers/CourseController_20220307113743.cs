using Group3.Helpper;
using Group3.Models;
using Group3.Models.DAO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Group3.Controllers
{
    public class CourseController:Controller
    {
        CourseDAO dao = new CourseDAO();
        public IActionResult CourseAdmin(){
            ViewData["Courses"]=Courses().toList();
            return View();
        }
        public List<Course> Courses(){
            return dao.GetCourses();
        }
    }
}