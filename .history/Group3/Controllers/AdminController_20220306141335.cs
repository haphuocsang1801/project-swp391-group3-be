﻿using Group3.Models.DAO;
using Microsoft.AspNetCore.Mvc;

namespace Group3.Controllers
{
    public class AdminController : Controller
    {
        public IActionResult Index()
        {
            var check = HttpContext.Session.GetString("UserID");
            if (check == null)
            {
                check = "0";    
            }
            int userID = int.Parse(check);
            UserDAO dao = new UserDAO();
            string? roleID = dao.getRoleUser(userID);
            if (roleID != "1")
            {
                return RedirectToAction("Index", "Home");
            }
            TempData["totleUses"]=getTotalStudent();

            
            return View();
        }
        public int getTotalStudent(){
            UserDAO obj = new UserDAO();
            return obj.getTotalUser();
            
        }
    }
}
