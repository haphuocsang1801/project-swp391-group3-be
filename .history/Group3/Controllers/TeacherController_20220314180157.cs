﻿using Group3.Models.DAO;
using Microsoft.AspNetCore.Mvc;
using Group3.Models;

namespace Group3.Controllers
{
    public class TeacherController : Controller
    {
        UserDAO dao = new UserDAO();
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult TeacherAdmin(){
            ViewData["Teachers"]=TeacherDAO.getTeacherList();
            ViewData["Users"]=dao.getListUser();
            ViewData["Waiting"]=WaitingDAO.getWaitingTeacher();
            return View();
        }
        public JsonResult TeacherAdminJson(string Value){

            getTeachersAndUsers obj = new getTeachersAndUsers();
            List<User> list = new List<User>(); 
            obj.UserList=dao.getListUser();
            obj.Teacher=TeacherDAO.GetTeacher(int.Parse(Value));
            return Json(obj);
        }
        public JsonResult ApproveTeacher(string Value,string ID){
            bool check=Value.Equals("Approve")?true:false;
            if(check){
                TeacherDAO.UpdateRoleUserToTeacher(int.Parse(ID));
                TeacherDAO.UpdateIsActiveTeacher(int.Parse(ID));
                WaitingDAO.UpdateStatus(int.Parse(ID));
            }
            return Json(new{
                redirectUrl = Url.Action("TeacherAdmin", "Teacher"),
                isRedirect = true 
            });
        }
        public JsonResult GetTeacherAdmin(string Value){

            getListTeacherAndUser obj = new getListTeacherAndUser();
            List<User> list = new List<User>(); 
            obj.UserList=dao.getListUser();
            obj.Teachers=TeacherDAO.GetTeacherSearch(Value);
            obj.idTeacher=TeacherDAO.GetTeacherActive();
            return Json(obj);
        }
        public JsonResult FeedbackTeacher(string Value,string id){
            string date = DateTime.UtcNow.ToString("MM-dd-yyyy");
            var userID = HttpContext.Session.GetString("UserID");
            return Json(new{
                redirectUrl = Url.Action("TeacherAdmin", "Teacher"),
                isRedirect = true 
            });
        }
    }
    public class getTeachersAndUsers{
            public IEnumerable<User> UserList { get; set; }
            public Teacher Teacher { get; set; }
    }
    public class getListTeacherAndUser{
            public IEnumerable<User> UserList { get; set; }
            public IEnumerable<Teacher> Teachers { get; set; }
            public IEnumerable<int> idTeacher{get;set;}
    }
}
