﻿using Group3.Helpper;
using Group3.Models;
using Group3.Models.DAO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Group3.Controllers
{
    public class AccountController : Controller
    {
        public IActionResult Login()
        {
            var check = HttpContext.Session.GetString("UserID");
            if (check == null)
            {
                return View();
            }
            int? userID = int.Parse(check);
            UserDAO dao = new UserDAO();
            string? roleID = dao.getRoleUser(userID);
            if (roleID == "1")
            {
                return RedirectToAction("Index", "Admin");
            }
            if (roleID.Equals("2"))//teacher
            {
                return RedirectToAction("Index", "Teacher");
            }
            if (roleID.Equals("3"))//user
            {
                return RedirectToAction("Index", "User");
            }
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CheckLogin(string name, string password)
        {
            string username = name;
            string userpassword = password;
            if (username == null && userpassword == null)
            {
                TempData["Error"] = "Please enter your user name and password";
                return RedirectToAction("Login", "Account");
            }
            else
            {
                UserDAO obj = new UserDAO();
                User? user = obj.getUser(username, userpassword);
                if (user == null)
                {
                    TempData["Error"] = "User name or Password incorrect";
                    return RedirectToAction("Login", "Account");
                }
                if (user != null)
                {
                    HttpContext.Session.SetString("UserID", user.UserID.ToString());
                    if (user.roleID == "1")//admin
                    {
                        return RedirectToAction("Index", "Admin");
                    }
                    if (user.roleID.Equals("2"))//teacher
                    {
                        return RedirectToAction("Index", "Teacher");
                    }
                    if (user.roleID.Equals("3"))//user
                    {
                        return RedirectToAction("Index", "User");
                    }
                }

            }
            return null;
        }
        public IActionResult Sigup()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateAccount(string username, string fullname, string password, string confirmPassword, string email, string address, string contact)
        {
            try
            {
                UserDAO obj = new UserDAO();
                if (checkValidUser(username, fullname, password, confirmPassword, email, address, contact))
                {
                    if(obj.getUserName(username)){
                        TempData["UserNameError"] = "UserName already exits".ToString();
                    }
                    return RedirectToAction("Sigup", "Account");
                }
                else
                {
                    DateTime today = DateTime.Today;
                    bool check = obj.InsertUser(username, fullname, email, password, address, contact, today);
                    if (check)
                    {
                        return RedirectToAction("Login", "Account");
                    }
                    else
                    {
                        TempData["Error"] = "Have a error";
                        return RedirectToAction("Sigup", "Account");
                    }
                }

            }
            catch (Exception)
            {

                return RedirectToAction("Sigup", "Account");
            }

        }
        public bool checkValidUser(string username, string fullname, string password, string confirmPassword, string email, string address, string contact)
        {
            bool checkConfirm = checkConformPassword(password, confirmPassword);
            bool checkEmail = Utilities.IsValidEmail(email);
            //bool checkNumberPhone = Utilities.IsPhoneNumber(contact);
            if (!checkConfirm)
            {
                TempData["PasswordError"] = "Password not match".ToString();
            }
            if (!checkEmail)
            {
                TempData["EmailError"] = "Invalid email address".ToString();
            }
            if (password.Length < 6)
            {
                TempData["PasswordLengthError"] = "Password too short".ToString();
            }
            /*if (checkNumberPhone)
            {
                TempData["ContactError"] = "Invalid number phone";
            }*/
            if (!checkConfirm || !checkEmail || password.Length < 6 /*||*/ /*checkNumberPhone==false*/)
            {
                return true;
            }
            return false;
        }
        public bool checkConformPassword(string password, string confirmPassword)
        {
            if (password == confirmPassword)
            {
                return true;
            }
            return false;
        }
    }
}
