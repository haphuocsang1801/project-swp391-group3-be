using Group3.Helpper;
using Group3.Models;
using Group3.Models.DAO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Group3.Controllers
{
    public class CourseController:Controller
    {
        CourseDAO dao = new CourseDAO();
        public IActionResult CourseAdmin(){
            ViewData["Courses"]=dao.GetCourses();
            ViewData["Users"]= new UserDAO().getListUser();
            ViewData["Teachers"]= TeacherDAO.getTeacherList();
            return View();
        }
        public JsonResult GetCourseAdmin(string SearchValue){
            List<Course> CourseList=new List<Course>();
            CourseList=dao.GetCourseByIDandName(SearchValue);
            List<User> UserList= new UserDAO().getListUser();
            ViewModelCourseUser obj= new ViewModelCourseUser();
            obj.Users=UserList;
            obj.Courses=CourseList;
            return Json(obj);
        }
    } 
    public class ViewModelCourseUser
        {
            public IEnumerable<User> Users { get; set; }
            public IEnumerable<Course> Courses { get; set; }
        }
}