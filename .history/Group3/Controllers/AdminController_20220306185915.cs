﻿using Group3.Models;
using Group3.Models.DAO;
using Microsoft.AspNetCore.Mvc;

namespace Group3.Controllers
{
    public class AdminController : Controller
    {
        public IActionResult Index()
        {
            var check = HttpContext.Session.GetString("UserID");
            if (check == null)
            {
                check = "0";    
            }
            int userID = int.Parse(check);
            UserDAO dao = new UserDAO();
            string? roleID = dao.getRoleUser(userID);
            if (roleID != "1")
            {
                return RedirectToAction("Index", "Home");
            }
            TempData["totalUser"]=getTotalStudent().ToString();
            TempData["totalCourse"]=getTotalCourse().ToString();
            TempData["totalPayment"]=getTotalPayment().ToString();
            
            return View(getListPayment());
        }
        public int getTotalStudent(){
            UserDAO obj = new UserDAO();
            return obj.getTotalUser();
        }
        public int getTotalCourse(){
            CourseDAO obj= new CourseDAO();
            return obj.totalCourse();
        }
        public int getTotalPayment(){
            PaymentDAO dao = new PaymentDAO();
            return dao.totalPayment();
        }
        public List<Payment> getListPayment(){
            PaymentDAO dao = new PaymentDAO();
            return dao.getListUserPayment();
        }
        public List<User> getListUser(){
            UserDAO obj = new UserDAO();
        }
    }
}
