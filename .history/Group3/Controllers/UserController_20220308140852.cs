﻿using Microsoft.AspNetCore.Mvc;
using Group3.Models.DAO;
using Group3.Models;
namespace Group3.Controllers
{
    public class UserController : Controller
    {
        UserDAO dao = new UserDAO();
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult UserAdmin(){
            List<User> Userlist =  dao.getListUser();
            List<Fee> FeeList=FeeDAO.getFeeList();
            ViewData["Users"]=Userlist;
            ViewData["Fees"]=FeeList;
            ViewData["Subscriptions"]=SubscriptionDAO.getSubscriptionList();
            return View();
        }
    }
}
