using Group3.Helpper;
using Group3.Models;
using Group3.Models.DAO;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Group3.Controllers
{
    public class CourseController:Controller
    {
        CourseDAO dao = new CourseDAO();
        public IActionResult CourseAdmin(){
            ViewData["Courses"]=Courses();
            ViewData["Users"]= new UserDAO().getListUser();
            ViewData["Teachers"]= TeacherDAO.getTeacherList();
            return View();
        }
        public JsonResult GetCourseAdmin(string SearchValue){
            List<Course> CourseList=new List<Course>();
            CourseList=dao.GetCourseByIDandName(SearchValue);
            List<User> UserList= new UserDAO().getListUser();
            return Json(CourseList);
        }
        public List<Course> Courses(){
            return dao.GetCourses();
        }
    }
    public static class ViewModelCourseUser
        {
            public static IEnumerable<User> Users { get; set; }
            public static IEnumerable<Course> Courses { get; set; }
        }
}