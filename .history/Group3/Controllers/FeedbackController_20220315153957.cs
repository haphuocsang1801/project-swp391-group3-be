using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Group3.Models.DAO;
//using Group3.Models;

namespace Group3.Controllers
{
    public class FeedbackController : Controller
    {
        public FeedbackController()
        {
        }

        public IActionResult Index()
        {
            ViewData["Feedbacks"]=FeedbackDAO.GetFeedback("");
            return View();
        }
    }
}